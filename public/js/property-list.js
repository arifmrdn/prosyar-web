var current_page = 1;
var max_page = 1;
function getListProperty(_page = current_page, _q = '', _type = ''){
    $.get({
        type: "GET",
        url: '/property/get/list/all',
        data: {
            page: _page,
            q: _q,
            type: _type
        },
    })
    .done(function(result){
        $('.property-list-container').append(result.view);
        max_page = result.total_page;
    });

    return false;
}