var current_page = 1;
var max_page = 1;
function getGridProperty(_page = current_page, _q = '', _type = '', _loc = ''){
    $.get({
        type: "GET",
        url: '/property/get/grid/all',
        data: {
            page: _page,
            q: _q,
            type: _type,
            loc: _loc
        },
    })
    .done(function(result){
        $('.property-grid-container').append(result.view);
        max_page = result.total_page;
    });

    return false;
}