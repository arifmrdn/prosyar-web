<?php

namespace App\Helpers;

class Helper
{
    public static function checkImagePath($path)
    {
        return (isset($path)) && file_exists(storage_path('app/'.$path)) ? $path : asset('placeholder.png');
    }

    public static function checkProfileImg($path)
    {
        return (isset($path)) && file_exists(storage_path('app/'.$path)) ? $path : asset('default-profile.png');
    }
}
