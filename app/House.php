<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $fillable = [
        'nama', 'id_property', 'harga', 'alamat', 'tipe', 'panjang', 'lebar',
        'listrik', 'sertifikat', 'terdaftar', 'kamar_tidur', 'kamar_mandi', 'ruang_tamu', 'dapur',
        'denah', 'tempat_parkir', 'sudah_ditempati', 'deskripsi', 'locations_id', 'contacts_id', 'gambar'
    ];

    public $timestamps = false;

    protected $casts = [
        'gambar' => 'json',
    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact', 'contacts_id');
    }
}
