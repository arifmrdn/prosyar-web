<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\House;
use App\Contact;
use App\Location;
use DB;

class HouseController extends Controller
{
    public function showPropertyList(Request $request)
    {
        $houses = House::paginate(2);
        if($request->q != '' && $request->type != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)->paginate(2);
        }elseif($request->q != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])->paginate(2);
        }elseif($request->type != ''){
            $houses = House::where('tipe', $request->type)->paginate(2);
        }

        $data = [
            'houses' => $houses,
            'total_page' => $houses->total()
        ];
        return view('property-list', $data);
    }

    public function showPropertyGrid(Request $request)
    {
        $houses = House::paginate(2);
        if($request->loc != ''){
            $location = Location::where('nama', $request->loc)->first();
        }

        if($request->q != '' && $request->type != '' && isset($location)){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)
                        ->where('locations_id', $location->id)
                        ->paginate(2);
        }elseif($request->q != '' && $request->type != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)->paginate(2);
        }elseif($request->q != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])->paginate(2);
        }elseif($request->type != ''){
            $houses = House::where('tipe', $request->type)->paginate(2);
        }elseif(isset($location)){
            $houses = House::where('locations_id', $location->id)->paginate(2);
        }

        $data = [
            'houses' => $houses,
            'total_page' => $houses->total()
        ];
        return view('property-grid', $data);
    }

    public function show($id)
    {
        $house = House::find($id);
        
        return view('property-description', compact('house'));
    }

    public function getPropertyListAll(Request $request)
    {
        $houses = House::paginate(2);
        if($request->q != '' && $request->type != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)->paginate(2);
        }elseif($request->q != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])->paginate(2);
        }elseif($request->type != ''){
            $houses = House::where('tipe', $request->type)->paginate(2);
        }

        $result = [
            'view' => view('partials._property-list', compact('houses'))->render(),
            'total_page' => $houses->total()
        ];
        return $result;
    }
    public function getPropertyGridAll(Request $request)
    {
        $houses = House::paginate(2);
        if($request->loc != ''){
            $location = Location::where('nama', $request->loc)->first();
        }

        if($request->q != '' && $request->type != '' && isset($location)){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)
                        ->where('locations_id', $location->id)
                        ->paginate(2);
        }elseif($request->q != '' && $request->type != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])
                        ->where('tipe', $request->type)->paginate(2);
        }elseif($request->q != ''){
            $houses = House::where([['alamat', 'LIKE', '%'.$request->q.'%']])->paginate(2);
        }elseif($request->type != ''){
            $houses = House::where('tipe', $request->type)->paginate(2);
        }elseif(isset($location)){
            $houses = House::where('locations_id', $location->id)->paginate(2);
        }

        $result = [
            'view' => view('partials._property-list', compact('houses'))->render(),
            'total_page' => $houses->total()
        ];
        return $result;
    }

    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->add(['terdaftar' => date('Y-m-d')]);
        $gambar_arr = [];

        foreach($request->gambar as $file){
            $file->move(storage_path('app/uploads/property'), $file->getClientOriginalName());
            $gambar_arr[] = 'uploads/property/'.$file->getClientOriginalName();
        }
        $gambar =  json_encode($gambar_arr);

        $request->denah->move(storage_path('app/uploads/denah'), $request->denah->getClientOriginalName());
        $denah = 'uploads/denah/'.$request->denah->getClientOriginalName();

        House::create([
            'gambar' => $gambar,
            'id_property' => $request->id_property,
            'nama' => $request->nama,
            'harga' => $request->harga,
            'alamat' => $request->alamat,
            'tipe' => $request->tipe,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'listrik' => $request->listrik,
            'sertifikat' => $request->sertifikat,
            'terdaftar' => $request->terdaftar,
            'kamar_tidur' => $request->kamar_tidur,
            'kamar_mandi' => $request->kamar_mandi,
            'dapur' => $request->dapur,
            'denah' => $denah,
            'tempat_parkir' => $request->tempat_parkir,
            'sudah_ditempati' => $request->sudah_ditempati,
            'deskripsi' => $request->deskripsi,
            'locations_id' => $request->locations_id,
            'contacts_id' => $request->contacts_id,
            'ruang_tamu' => $request->ruang_tamu,
        ]);
        
        $message = [
            'message' => 'Berhasil menambahkan properti',
            'type' => 'success'
        ];
        
        return redirect()->back()->with($message);
    }
}
