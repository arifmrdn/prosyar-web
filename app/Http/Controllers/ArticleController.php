<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Contact;

class ArticleController extends Controller
{
    public function showArticle()
    {
        $articles = Article::all();

        return view('article', compact('articles'));
    }
    public function show($id)
    {
        $articles = Article::find($id);
        $contacts = Contact::where('nama', 'Angga')->first();
        
        return view('article-description', compact('articles', 'contact'));
    }
}
