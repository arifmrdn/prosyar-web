<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function showContact()
    {
        $contacts = Contact::all();
        return view('contact', compact('contacts'));
    }
}
