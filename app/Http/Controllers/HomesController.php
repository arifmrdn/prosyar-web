<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Home;
use App\House;
use App\Contact;

class HomesController extends Controller
{
    public function showHomes()
    {
        $homes  = Home::all();
        $houses = House::limit(6)->get();
        $contacts= Contact::all();

        return view('home', compact('homes', 'houses', 'contacts'));
    }
}
