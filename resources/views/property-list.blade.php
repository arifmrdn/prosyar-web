@extends('layouts.property')

@section('property')

<main class="body" role="main">
  @include('partials.search')
  <!-- Filters Section -->
  <div class="container space-1">
    <!-- Title -->
    <div class="mb-5">
      <h1 class="h4 font-weight-medium search-text"></h1>
    </div>
    <!-- End Title -->
  </div>
  <!-- End Filters Section -->
    <!-- ========== MAIN CONTENT ========== -->
    <!-- List of Properties Section -->
    <div class="container space-top-1 space-bottom-2">
      <div class="row">
        <div class="col-lg-12 mx-auto property-list-container">
            @foreach($houses as $house)
            <!-- Property Item -->
            <div class="border-bottom pb-5 mb-5">
              <div class="row">
                  <div class="col-md-4 d-md-flex align-items-md-start flex-md-column mb-5 mb-md-0">
                  <!-- Gallery -->
                  <a class="js-fancybox u-media-viewer mb-3" href="javascript:;"
                      data-src="{{ Helper::checkImagePath(json_decode($house->gambar)[0]) }}"
                      data-fancybox="fancyboxGallery{{$house->id}}"
                      data-caption="Front in frames - image #01"
                      data-speed="700"
                      data-is-infinite="true">
                      <img class="img-fluid w-100" src="{{ Helper::checkImagePath(json_decode($house->gambar)[0]) }}" alt="Image Description">
          
                      <div class="position-absolute bottom-0 right-0 pb-2 pr-2">
                      <span class="btn btn-icon btn-sm btn-white">
                          <span class="fas fa-images btn-icon__inner"></span>
                      </span>
                      </div>
                  </a>
                  
                  @if(count($house->gambar) > 0)
                  @foreach (json_decode($house->gambar) as $gambar)
                    @if($loop->iteration > 1)
                    <img class="js-fancybox d-none" alt="Image Description"
                          data-fancybox="fancyboxGallery{{$house->id}}"
                          data-src="{{Helper::checkImagePath($gambar)}}"
                          data-caption="Front in frames - image #02"
                          data-speed="700"
                          data-is-infinite="true">
                    @endif
                  @endforeach
                  @endif
                  <!-- End Gallery -->
          
                  <!-- Agent Info -->
                  <div class="media align-items-center mt-auto">
                      <div class="u-sm-avatar mr-2">
                      <img class="img-fluid rounded-circle" src="{{ Helper::checkProfileImg($house->contact->gambar) }}" alt="Image Description" title="{{ $house->contact->nama }}">
                      </div>
                      <div class="media-body">
                        <small class="d-block text-muted">Terdaftar Pada {!! $house->terdaftar !!}</small>
                        <span class="d-block">{{ $house->contact->nama }}</span>
                      </div>
                  </div>
                  <!-- End Agent Info -->
                  </div>
          
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-8">
                        <h1 class="h4 font-weight-bold mb-1">
                          <a href="{{ route('show.house', $house->id) }}">{!! $house->nama !!}</a>
                        </h1>
                        <h2 class="h4 mb-1">Rp{!! number_format($house->harga, 0,',','.'); !!}</h2>
                      </div>
                      <div class="col-4 text-right">
                        <span class="badge badge-success">{!! $house->tipe !!}</span>
                      </div>
                  </div>
          
                  <!-- Location -->
                  <div class="mb-3">
                      <a class="font-size-1" href="property-description.html">
                      <span class="fas fa-map-marker-alt mr-1"></span>
                      {!! $house->alamat !!}
                      </a>
                  </div>
                  <!-- End Location -->
          
                  <!-- Icon Blocks -->
                  <ul class="list-inline font-size-1">
                      <li class="list-inline-item mr-3" title="1 bedroom">
                      <span class="fas fa-bed text-muted mr-1"></span>
                      {!! $house->kamar_tidur !!}
                      </li>
                      <li class="list-inline-item mr-3" title="1 bathroom">
                      <span class="fas fa-bath text-muted mr-1"></span>
                      {!! $house->kamar_mandi !!}
                      </li>
                      <li class="list-inline-item mr-3" title="1 living room">
                      <span class="fas fa-couch text-muted mr-1"></span>
                      {!! $house->ruang_tamu !!}
                      </li>
                      <li class="list-inline-item mr-3" title="square feet">
                      <span class="fas fa-ruler-combined text-muted mr-1"></span>
                      {!! ($house->panjang * $house->lebar).' m<sup>2</sup>' !!}
                      </li>
                  </ul>
                  <!-- End Icon Blocks -->
          
                  <p class="font-size-1">{!! $house->deskripsi !!}</p>
          
                  <!-- Contacts -->
                  <div class="d-flex align-items-center font-size-1">
                      <a class="text-secondary mr-4" href="javascript:;">
                      <span class="fas fa-phone mr-1"></span>
                      {!! $house->contact->nomor_telepon !!}
                      </a>
                      <a class="text-secondary mr-4" href="javascript:;">
                      <span class="fas fa-envelope mr-1"></span>
                      Contact
                      </a>
                      <a class="btn btn-sm btn-soft-primary transition-3d-hover ml-auto" href="{{ route('show.house', $house->id) }}">
                      Lihat Detail
                      <span class="fas fa-angle-right ml-1"></span>
                      </a>
                  </div>
                  <!-- End Contacts -->
                  </div>
              </div>
            </div>
            <!-- End Property Item -->
        @endforeach
        </div>
      </div>
    </div>
    <!-- End List of Properties Section -->
  </main>
  <!-- ========== END MAIN CONTENT ========== -->

  @endsection
  @push('scripts')
    <script src="{{ asset('js/property-list.js') }}"></script>
    <script>
        $(document).ready(function(){
          //
        });
    </script>
  @endpush
</main>