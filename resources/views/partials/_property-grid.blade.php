@foreach ($houses as $house)
    <!-- Property Item -->
    <div class="card mb-5">
        <!-- Gallery -->
        <a class="js-fancybox u-media-viewer" href="javascript:;"
            data-src="../../assets/img/1920x1080/img36.jpg"
            data-fancybox="fancyboxGallery1"
            data-caption="Front in frames - image #01"
            data-speed="700"
            data-is-infinite="true">
        <img class="card-img-top w-100" src="{{ asset('assets/img/480x320/img19.jpg') }}" alt="Image Description">

        <div class="position-absolute top-0 left-0 pt-2 pl-3">
            <span class="badge badge-success">{!! $house->tipe !!}</span>
        </div>

        <div class="position-absolute bottom-0 left-0 right-0 pb-2 px-3">
            <div class="row justify-content-between align-items-center">
            <div class="col-8">
                <h2 class="h5 text-white mb-0">Rp{!! number_format($house->harga, 0,',','.'); !!}</h2>
            </div>

            <div class="col-4 text-right">
                <span class="btn btn-icon btn-sm btn-white">
                <span class="fas fa-images btn-icon__inner"></span>
                </span>
            </div>
            </div>
        </div>
        </a>

        <img class="js-fancybox d-none" alt="Image Description"
            data-fancybox="fancyboxGallery1"
            data-src="../../assets/img/1920x1080/img37.jpg"
            data-caption="Front in frames - image #02"
            data-speed="700"
            data-is-infinite="true">
        <img class="js-fancybox d-none" alt="Image Description"
            data-caption="Front in frames - image #03"
            data-src="../../assets/img/1920x1080/img38.jpg"
            data-fancybox="fancyboxGallery1"
            data-speed="700"
            data-is-infinite="true">
        <!-- End Gallery -->

        <div class="card-body p-4">
        <!-- Location -->
        <div class="mb-3">
            <a class="font-size-1" href="property-description.html">
            <span class="fas fa-map-marker-alt mr-1"></span>
            {!! $house->alamat !!}
            </a>
        </div>
        <!-- End Location -->

        <!-- Icon Blocks -->
        <ul class="list-inline font-size-1">
            <li class="list-inline-item mr-3" title="1 bedroom">
            <span class="fas fa-bed text-muted mr-1"></span>
            {!! $house->kamar_tidur !!}
            </li>
            <li class="list-inline-item mr-3" title="1 bathroom">
            <span class="fas fa-bath text-muted mr-1"></span>
            {!! $house->kamar_mandi !!}
            </li>
            <li class="list-inline-item mr-3" title="1 living room">
            <span class="fas fa-couch text-muted mr-1"></span>
            {!! $house->ruang_tamu !!}
            </li>
            <li class="list-inline-item mr-3" title="square feet">
            <span class="fas fa-ruler-combined text-muted mr-1"></span>
            {!! ($house->panjang * $house->lebar).' m<sup>2</sup>' !!}
            </li>
        </ul>
        <!-- End Icon Blocks -->

        <!-- Posted Info -->
        <div class="media align-items-center border-top border-bottom py-3 mb-3">
            <div class="u-avatar mr-3">
            <img class="img-fluid rounded-circle" src="../../assets/img/100x100/img1.jpg" alt="Image Description" title="Monica Fox">
            </div>
            <div class="media-body">
            <small class="d-block text-muted">Terdaftar pada {!! $house->terdaftar_pada !!}</small>
            <span class="d-block">Angga</span>
            </div>
        </div>
        <!-- End Posted Info -->

        <!-- Contacts -->
        <div class="d-flex align-items-center font-size-1">
            <a class="btn btn-sm btn-soft-primary transition-3d-hover ml-auto" href="{{ route('show.house', $house->id) }}">
            Lihat Detail
            <span class="fas fa-angle-right ml-1"></span>
        </a>
        </div>
        <!-- End Contacts -->
        </div>
    </div>

    @endforeach