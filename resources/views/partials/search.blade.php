<!-- Search Section -->
<form action="">
    <div class="bg-light">
    <div class="container space-1">
    <div class="row mx-gutters-2">
    <div class="col-lg mb-3 mb-lg-0">
        <!-- Search Property Input -->
        <div class="js-focus-state">
        <label class="sr-only" for="searchPropertySr">Cari property</label>
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
            <span class="input-group-text" id="searchProperty">
                <span class="fas fa-search"></span>
            </span>
            </div>
            <input type="text" class="form-control" name="q" id="searchPropertySr" placeholder="Search property" aria-label="Search property" @if(Request::get('q') != '') value="{{Request::get('q')}}" @endif aria-describedby="searchProperty">
        </div>
        </div>
        <!-- End Search Property Input -->
    </div>

        <div class="col-sm-auto ml-md-auto mb-3 mb-lg-0">
            <div class="input-group">
                <select name="type" class="form-control form-control-sm">
                    <option value="">Tipe Properti</option>
                    <option value="rumah" @if(Request::get('type') != '' && Request::get('type') == 'rumah') selected @endif>Rumah</option>
                    <option value="flat" @if(Request::get('type') != '' && Request::get('type') == 'flat') selected @endif>Flat</option>
                    <option value="multi keluarga" @if(Request::get('type') != '' && Request::get('type') == 'multi keluarga') selected @endif>Multi Keluarga</option>
                </select>
            </div>
            <!-- End Filter -->
    </div>

    <div class="col-sm-auto">
        @if(Request::get('loc') != '')
            <input type="hidden" name="loc" value="{{Request::get('loc')}}">
        @endif
        <button type="submit" class="btn btn-sm btn-primary">Cari</button>
    </div>
    </div>
    </div>
    </div>
</form>
<!-- End Search Section -->