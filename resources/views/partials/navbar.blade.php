<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header--navbar-bg">
  <div class="u-header__section bg-primary">
    
    <div id="logoAndNav" class="container">
      <!-- Nav -->
      <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
        <!-- Logo -->
        <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-text-white" href="/" aria-label="Front">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="46px" height="46px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
            <path fill="#E1E4EA" opacity=".65" d="M23,41.1L23,41.1c-9.9,0-18-8-18-18l0,0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6v11.4C41,33,32.9,41.1,23,41.1z"/>
            <path class="fill-white" opacity=".5" d="M28,36L28,36c-9.9,0-18-8-18-18l0,0c0-9.9,8-18,18-18h11.3C43,0.1,46,3.1,46,6.7v11.4C46,28,38,36,28,36z"/>
            <path class="fill-white" opacity=".7" d="M18,46.1L18,46.1c-10,0-18-8-18-18l0,0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6v11.4C35.9,38.1,27.9,46.1,18,46.1z"/>
            <path class="fill-primary" d="M17.4,34.1V18.4h10.2v2.9h-6.4v3.4H26v2.9h-4.8v6.5H17.4z"/>
          </svg>
          <span class="u-header__navbar-brand-text">Iprosyar</span>
        </a>
        <!-- End Logo -->

        <!-- Responsive Toggle Button -->
        <button type="button" class="navbar-toggler btn u-hamburger"
                aria-label="Toggle navigation"
                aria-expanded="false"
                aria-controls="navBar"
                data-toggle="collapse"
                data-target="#navBar">
          <span id="hamburgerTrigger" class="u-hamburger__box">
            <span class="u-hamburger__inner"></span>
          </span>
        </button>
        <!-- End Responsive Toggle Button -->

        <!-- Navigation -->
        <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
          <ul class="navbar-nav u-header__navbar-nav">
            <!-- Home -->
            <li class="nav-item u-header__nav-item">
              <a class="nav-link u-header__nav-link" id="home" href="{{ url('/home') }}">Home</a>
            </li>
            <!-- End Home -->

            <!-- Pages -->
            <li class="nav-item hs-has-sub-menu u-header__nav-item"
                data-event="hover"
                data-animation-in="slideInUp"
                data-animation-out="fadeOut">
              <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">Property</a>

              <!-- Pages - Submenu -->
              <ul id="pagesSubMenu" class="hs-sub-menu u-header__sub-menu" aria-labelledby="pagesMegaMenu" style="min-width: 230px;">
                <li><a class="nav-link u-header__sub-menu-nav-link" id="listing" href="{{ url('/property-list') }}">Listing</a></li>
                <li><a class="nav-link u-header__sub-menu-nav-link" id="listgrid" href="{{ url('/property-grid') }}">Listing (Grid)</a></li>
              </ul>
              <!-- End Pages - Submenu -->
            </li>
            <!-- End Pages -->
            
            <!-- Pages -->
            <li class="nav-item hs-has-sub-menu u-header__nav-item"
                data-event="hover"
                data-animation-in="slideInUp"
                data-animation-out="fadeOut">
              <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">Lokasi</a>

              <!-- Pages - Submenu -->
              <ul id="pagesSubMenu" class="hs-sub-menu u-header__sub-menu" aria-labelledby="pagesMegaMenu" style="min-width: 230px;">
                  @foreach($location_menus as $location)
                    <li><a class="nav-link u-header__sub-menu-nav-link" href="{{ url('/property-grid?loc='.strtolower($location->nama)) }}">{{ucwords($location->nama)}}</a></li>
                  @endforeach
              </ul>
              <!-- End Pages - Submenu -->
            </li>
            <!-- End Pages -->
    
            <!-- Contac -->
            <li class="nav-item u-header__nav-item">
            <a class="nav-link u-header__nav-link" id="contact" href="{{ route('index.contact') }}">Kontak Kami</a>
            </li>
            <!-- End Contac -->
    
          </ul>
        </div>
        <!-- End Navigation -->
      </nav>
      <!-- End Nav -->
    </div>
  
  </div>
</header>
<!-- ========== END HEADER ========== -->