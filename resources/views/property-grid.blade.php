@extends('layouts.property')

@section('property')


<main class="body" role="main">
  @include('partials.search')
  @if(Request::get('loc') != '')
    <!-- Filters Section -->
    <div class="container space-1">
      <!-- Title -->
        <div class="mb-5">
          <h1 class="h4 font-weight-medium">Property di {!! ucwords(Request::get('loc')) !!}</h1>
        </div>
      <!-- End Title -->
    </div>
  @endif
    <!-- End Filters Section -->
      <!-- List of Properties Section -->
      <div class="container space-top-1 space-bottom-3">
        <div class="row">
          <div class="col-lg-12">
            <!-- Card -->
            <div class="card-deck d-block d-md-flex property-grid-container">
                @foreach ($houses as $house)
                <!-- Property Item -->
                <div class="card mb-5">
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                        data-src="{{ Helper::checkImagePath(json_decode($house->gambar)[0]) }}"
                        data-fancybox="fancyboxGallery1"
                        data-caption="Front in frames - image #01"
                        data-speed="700"
                        data-is-infinite="true">
                    <img class="card-img-top w-100" src="{{ Helper::checkImagePath(json_decode($house->gambar)[0]) }}" alt="Image Description">
            
                    <div class="position-absolute top-0 left-0 pt-2 pl-3">
                        <span class="badge badge-success">{!! $house->tipe !!}</span>
                    </div>
            
                    <div class="position-absolute bottom-0 left-0 right-0 pb-2 px-3">
                        <div class="row justify-content-between align-items-center">
                        <div class="col-8">
                            <h2 class="h5 text-white mb-0">Rp{!! number_format($house->harga, 0,',','.'); !!}</h2>
                        </div>
            
                        <div class="col-4 text-right">
                            <span class="btn btn-icon btn-sm btn-white">
                            <span class="fas fa-images btn-icon__inner"></span>
                            </span>
                        </div>
                        </div>
                    </div>
                    </a>
            
                    @if(count($house->gambar) > 0)
                    @foreach (json_decode($house->gambar) as $gambar)
                      @if($loop->iteration > 1)
                      <img class="js-fancybox d-none" alt="Image Description"
                            data-fancybox="fancyboxGallery{{$house->id}}"
                            data-src="{{Helper::checkImagePath($gambar)}}"
                            data-caption="Front in frames - image #02"
                            data-speed="700"
                            data-is-infinite="true">
                      @endif
                    @endforeach
                    @endif
            
                    <div class="card-body p-4">
                    <!-- Location -->
                    <div class="mb-3">
                        <a class="font-size-1" href="property-description.html">
                        <span class="fas fa-map-marker-alt mr-1"></span>
                        {!! $house->alamat !!}
                        </a>
                    </div>
                    <!-- End Location -->
            
                    <!-- Icon Blocks -->
                    <ul class="list-inline font-size-1">
                        <li class="list-inline-item mr-3" title="1 bedroom">
                        <span class="fas fa-bed text-muted mr-1"></span>
                        {!! $house->kamar_tidur !!}
                        </li>
                        <li class="list-inline-item mr-3" title="1 bathroom">
                        <span class="fas fa-bath text-muted mr-1"></span>
                        {!! $house->kamar_mandi !!}
                        </li>
                        <li class="list-inline-item mr-3" title="1 living room">
                        <span class="fas fa-couch text-muted mr-1"></span>
                        {!! $house->ruang_tamu !!}
                        </li>
                        <li class="list-inline-item mr-3" title="square feet">
                        <span class="fas fa-ruler-combined text-muted mr-1"></span>
                        {!! ($house->panjang * $house->lebar).' m<sup>2</sup>' !!}
                        </li>
                    </ul>
                    <!-- End Icon Blocks -->
            
                    <!-- Posted Info -->
                    <div class="media align-items-center border-top border-bottom py-3 mb-3">
                        <div class="u-avatar mr-3">
                        <img class="img-fluid rounded-circle" src="{{Helper::checkProfileImg($house->contact->gambar)}}" alt="Image Description" title="Monica Fox">
                        </div>
                        <div class="media-body">
                        <small class="d-block text-muted">Terdaftar pada {!! $house->terdaftar !!}</small>
                        <span class="d-block">Angga</span>
                        </div>
                    </div>
                    <!-- End Posted Info -->
            
                    <!-- Contacts -->
                    <div class="d-flex align-items-center font-size-1">
                        <a class="btn btn-sm btn-soft-primary transition-3d-hover ml-auto" href="{{ route('show.house', $house->id) }}">
                        Lihat Detail
                        <span class="fas fa-angle-right ml-1"></span>
                    </a>
                    </div>
                    <!-- End Contacts -->
                    </div>
                </div>
            
                @endforeach
            </div>
            <!-- End Card -->
          </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End List of Properties Section -->
    </main>
  
  
  @endsection
  @push('scripts')
    <script src="{{ asset('js/property-grid.js') }}"></script>
    <script>
        $(document).ready(function(){
          $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
              var scrollPosition = $(window).height() + $(window).scrollTop();
              if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                if(current_page <= max_page){
                  getGridProperty(current_page += 1, $('#searchPropertySr').val(), $('[name="propertyRadioChechbox"]:checked').val(), $('[name="loc"]').val());
                }
                console.log(max_page);
              }
          });
        });
    </script>
  @endpush
</main>