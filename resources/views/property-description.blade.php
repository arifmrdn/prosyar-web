@extends('layouts.property')


@section('property-description')

    <main class="body" role="main">
      <!-- Gallery Section -->
      <div class="container space-1">
        @if(count($house->gambar) > 0)
        <div class="js-slick-carousel u-slick u-slick-zoom u-slick--gutters-3 mb-7"
        data-slides-show="3"
        data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
        data-responsive='[{
          "breakpoint": 992,
          "settings": {
            "slidesToShow": 2
          }
        }, {
          "breakpoint": 768,
          "settings": {
            "slidesToShow": 1
          }
        }]'>
          @foreach (json_decode($house->gambar) as $gambar)
          <a lass="js-fancybox u-media-viewer" href="javascript:;"
          data-src="{{asset($gambar)}}"
          data-fancybox="header-gallery"
          data-speed="700"
          data-is-infinite="true">
            <div class="js-slide card border-0 shadow-sm mb-3">
              <div class="position-relative">
                <img class="card-img-top" src="{{asset($gambar)}}" alt="Image Description">
              </div>
            </div>
          </a>
          @endforeach
        </div>
        @else
        <div class="js-slick-carousel u-slick u-slick-zoom u-slick--gutters-3 mb-0 mt-3"
        data-slides-show="3"
        data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
        data-responsive='[{
          "breakpoint": 992,
          "settings": {
            "slidesToShow": 2
          }
        }, {
          "breakpoint": 768,
          "settings": {
            "slidesToShow": 1
          }
        }]'>
          <a lass="js-fancybox u-media-viewer" href="javascript:;"
          data-src="{{asset('placeholder.png')}}"
          data-fancybox="header-gallery"
          data-speed="700"
          data-is-infinite="true">
            <div class="js-slide card border-0 shadow-sm mb-3">
              <div class="position-relative">
                <img class="card-img-top" src="{{asset('placeholder.png')}}" alt="Image Description">
              </div>
            </div>
          </a>
        </div>
        @endif
      </div>
    </div>
    <!-- End Gallery Section -->
  
      <!-- Property Description Section -->
      <div class="container space-bottom-2">
        <!-- Additional Functions -->
        <div class="d-md-flex justify-content-md-start align-items-md-center text-center mb-7">
          <div class="mr-md-auto mb-2 mb-md-0">
            <span class="font-size-1 font-weight-medium">Terdaftar Pada:</span>
            <span class="text-secondary font-size-1">{!! $house->terdaftar !!}</span>
          </div>
          <a class="btn btn-sm btn-outline-secondary border-white" href="javascript:;">
            <span class="fas fa-share-alt mr-2"></span>
            Share
          </a>
        </div>
        <!-- End Additional Functions -->
  
        <!-- Title -->
        <div class="row justify-content-lg-between">
          <div class="col-lg-8">
            <h1 class="h3 font-weight-medium">{!! $house->alamat !!}</h1>
          </div>
  
          <div class="col-lg-4 align-self-lg-end text-lg-right mb-5 mb-lg-0">
            <span class="h3 text-primary font-weight-medium">Rp.{!! number_format($house->harga, 0,',','.'); !!}</span>
          </div>
        </div>
        <!-- End Title -->
  
        <div class="row space-top-2">
          <div class="col-lg-8 mb-9 mb-lg-0">
            <!-- Nav Classic -->
            <ul id="SVGnavIcons" class="svg-preloader nav nav-classic nav-rounded nav-shadow nav-justified border" role="tablist">
              <li class="nav-item">
                <a class="nav-link font-weight-medium active" id="pills-one-tab" data-toggle="pill" href="#pills-one" role="tab" aria-controls="pills-one" aria-selected="true">
                  <div class="d-md-flex justify-content-md-center align-items-md-center">
                    <figure class="ie-height-40 d-none d-md-block w-100 max-width-6 mr-3">
                      <img class="js-svg-injector" src="../../assets/svg/icons/icon-13.svg" alt="SVG"
                           data-parent="#SVGnavIcons">
                    </figure>
                    Detail Rumah
                  </div>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link font-weight-medium" id="pills-two-tab" data-toggle="pill" href="#pills-two" role="tab" aria-controls="pills-two" aria-selected="false">
                  <div class="d-md-flex justify-content-md-center align-items-md-center">
                    <figure class="ie-height-40 d-none d-md-block w-100 max-width-6 mr-3">
                      <img class="js-svg-injector" src="../../assets/svg/icons/icon-63.svg" alt="SVG"
                           data-parent="#SVGnavIcons">
                    </figure>
                    Denah
                  </div>
                </a>
              </li>
            </ul>
            <!-- End Nav Classic -->
  
            <!-- Tab Content -->
            <div class="tab-content">
              <div class="tab-pane fade pt-6 show active" id="pills-one" role="tabpanel" aria-labelledby="pills-one-tab">
  
                <!-- Key Facts -->
                <div class="row">
                  <div class="col-md-6">
                    <!-- Fact List -->
                    <ul class="list-unstyled font-size-1 mb-0">
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Property ID:</span>
                        <span class="text-secondary">{!! $house->id_property !!}</span>
                      </li>
  
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Tipe:</span>
                        <span class="text-secondary">{!! $house->tipe !!}</span>
                      </li>
  
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Luas:</span>
                        <span class="text-secondary">{!! ($house->panjang * $house->lebar).' m<sup>2</sup>' !!}</span>
                      </li>
                    </ul>
                    <!-- End Fact List -->
                  </div>
  
                  <div class="col-md-6">
                    <!-- Fact List -->
                    <ul class="list-unstyled font-size-1 mb-0">
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Kamar Tidur:</span>
                        <span class="text-secondary">{!! $house->kamar_tidur !!}</span>
                      </li>
  
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Kamar Mandi:</span>
                        <span class="text-secondary">{!! $house->kamar_mandi !!}</span>
                      </li>
  
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Dapur:</span>
                        <span class="text-secondary">{!! $house->dapur !!}</span>
                      </li>
  
                      <li class="d-sm-flex justify-content-sm-between py-1">
                        <span class="font-weight-medium">Ruang Tamu:</span>
                        <span class="text-secondary">{!! $house->ruang_tamu !!}</span>
                      </li>
                    </ul>
                    <!-- End Fact List -->
                  </div>
                </div>
                <!-- End Key Facts -->
  
                <hr class="my-6">
  
                <!-- Title -->
                <div class="mb-3">
                  <h3 class="h5">Deskripsi</h3>
                </div>
                <!-- End Title -->
                
                <p class="collapse" id="collapseLinkExample">{!! $house->deskripsi !!}</p>
                
                <a class="link-collapse" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                  <span class="link-collapse__default">Lihat</span>
                  <span class="link-collapse__active">Tutup</span>
                </a>
                
                <hr class="my-6">
                
                <!-- Title -->
                <div class="mb-3">
                  <h3 class="h5 mb-1">Harga</h3>
                </div>
                <!-- End Title -->
  
                <!-- Estimated Costs -->
                <div class="row">
                  <div class="col-md-6">
                    <span class="h1 font-weight-medium mb-0">Rp.{!! number_format($house->harga, 0,',','.'); !!}</span>
                  </div>
  
                  <div class="col-md-6">
                    <!-- Costs List -->
                    <ul class="list-unstyled font-size-1 mb-0">
                      <li class="d-flex align-items-center py-1">
                        <span class="fas fa-burn min-width-4 text-secondary text-center mr-2"></span>
                        <span class="font-weight-medium">Listrik</span>
                        <div class="ml-auto">
                          <span class="text-secondary">{!! $house->listrik !!}</span>
                        </div>
                      </li>
  
                      <li class="d-flex align-items-center py-1">
                        <span class="fas fa-tint min-width-4 text-secondary text-center mr-2"></span>
                        <span class="font-weight-medium">Water</span>
                        <div class="ml-auto">
                         <span class="text-secondary"></span>
                        </div>
                      </li>
  
                      <li class="d-flex align-items-center py-1">
                        <span class="fas fa-shield-alt min-width-4 text-secondary text-center mr-2"></span>
                        <span class="font-weight-medium">Sertifikat</span>
                        <div class="ml-auto">
                          <span class="text-secondary">{!! $house->sertifikat == 1 ? '<span class="badge badge-success"><i class="fa fa-check"></i></span>' : '<span class="badge badge-default"><i class="fa fa-times-circle fa-lg"></i></span>' !!}</span>
                        </div>
                      </li>
                    </ul>
                    <!-- End Costs List -->
                  </div>
                </div>
                <!-- End Estimated Costs -->
              </div>
  
              <div class="tab-pane fade pt-6" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                
                <!-- Gallery -->
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                   data-src="{{Helper::checkImagePath($house->denah)}}"
                   data-fancybox="fancyboxGalleryFloorPlan"
                   data-caption="Floorplan image #01"
                   data-speed="700"
                   data-is-infinite="true">
                  <img class="img-fluid" src="{{Helper::checkImagePath($house->denah)}}" alt="Image Description">
                </a>
                <!-- End Gallery -->
  
                <div class="mt-2">
                  <p class="small">Image source from <a href="https://floorplanner.com/" target="_blank">floorplanner.com</a></p>
                </div>
              </div>
            </div>

            <!-- End Tab Content -->
          </div>
  
          <div class="col-lg-4">
            <div id="stickyBlockStartPoint" class="pl-lg-4">
              <!-- Contact Form -->
              <div class="js-sticky-block card shadow-sm p-4"
                   data-parent="#stickyBlockStartPoint"
                   data-sticky-view="lg"
                   data-start-point="#stickyBlockStartPoint"
                   data-end-point="#stickyBlockEndPoint"
                   data-offset-top="24"
                   data-offset-bottom="0">
                <!-- Header -->
                
                <div class="media align-items-center mb-4">
                 <!-- Agent -->

                 <div class="media">
                    <span class="btn btn-icon btn-lg mr-3">
                        <img src="{{ Helper::checkProfileImg($house->contact->gambar) }}" width="80px" height="80px" alt="gambar">
                    </span>
                      <div class="media-body">
                          <h4 class="h6 mb-1">{{ $house->contact->nama }}</h4>
                          <p class="font-size-1 mb-2">
                          <span class="fas fa-phone mr-1"></span>
                            {!! $house->contact->nomor_telepon !!}
                          </p>
                          <a class="btn btn-xs btn-soft-primary" href="#">Hubungi Saya</a>
                      </div>
                  </div>
                    <!-- End Agent -->
                </div>
                <!-- End Header -->
              </div>
              <!-- End Contact Form -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Property Description Section -->
    </main>
@endsection