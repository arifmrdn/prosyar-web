@extends('layouts.property')

@section('property')

    <!-- ========== MAIN ========== -->
    <main id="content" class="body" role="main">
        <!-- Hero Section -->
        <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll" data-options='{direction: "normal"}'>
            <!-- Apply your Parallax background image here -->
            <div class="divimage dzsparallaxer--target" style="height: 130%; background-image: url(../../assets/img/1920x1080/img1.jpg);"></div>
    
            <!-- Content -->
            <div class="js-scroll-effect position-relative"
                data-scroll-effect="smoothFadeToBottom">
                <div class="container space-top-2 space-bottom-1 space-top-md-5">
                    <div class="text-center w-lg-80 mx-auto space-bottom-2 space-bottom-md-3">
                        <h1 class="display-4 font-size-md-down-5 text-white font-weight-normal mb-0">Announcing a free plan for small teams</h1>
                    </div>
    
                <!-- Author -->
                <div class="text-center">
                    <div class="u-avatar mx-auto mb-2">
                        <img class="img-fluid rounded-circle" src="../../assets/img/100x100/img1.jpg" alt="Image Description">
                    </div>
                    <span class="d-block">
                        <a class="text-white h6 mb-0" href="#">Andrea Gard</a>
                    </span>
                </div>
                <!-- End Author -->
                </div>
            </div>
            <!-- End Content -->
        </div>
        <!-- End Hero Section -->
    
        <!-- Description Section -->
        <div class="container space-top-2 space-top-md-3">
            <div class="w-lg-60 mx-auto">
                <div class="mb-4">
                    <span class="text-muted">April 15, 2018</span>
                </div>
    
                <div class="mb-5">
                <p>At Front, our mission has always been focused on bringing openness and transparency to the design process. We've always believed that by providing a space where designers can share ongoing work not only empowers them to make better products, it also helps them grow. We're proud to be a part of creating a more open culture and to continue building a product that supports this vision.</p>
                <p>As we've grown, we've seen how Front has helped companies such as Spotify, Microsoft, Airbnb, Facebook, and Intercom bring their designers closer together to create amazing things. We've also learned that when the culture of sharing is brought in earlier, the better teams adapt and communicate with one another.</p>
                <p>That's why we are excited to share that we now have a <a href="#">free version of Front</a>, which will allow individual designers, startups and other small teams a chance to create a culture of openness early on.</p>
                </div>
        
                <h2 class="h5 mb-3">Bringing the culture of sharing to everyone.</h2>
        
                <p>Small teams and individual designers need a space where they can watch the design process unfold, both for themselves and for the people they work with – no matter if it's a fellow designer, product manager, developer or client. Front allows you to invite more people into the process, creating a central place for conversation around design. As those teams grow, transparency and collaboration becomes integrated in how they communicate and work together.</p>
            </div>
        </div>
        <!-- End Description Section -->
    </main>
    <!-- ========== END MAIN ========== -->

@endsection