<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Iprosyar</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="../../favicon.ico">

    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/hs-megamenu/src/hs.megamenu.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/chartist/dist/chartist.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fancybox/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">  
    <link rel="stylesheet" href="{{ asset('vendor/cubeportfolio/css/cubeportfolio.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/slick-carousel/slick/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dzsparallaxer/dzsparallaxer.css') }}">

    <!-- CSS Front Template -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
  </head>
  <body>
    @if(Request::route()->getName() == 'home')
      @include('partials.navbar-home')
    @else
      @include('partials.navbar')
    @endif
    
        @yield('property-description')
      
                                  
      @yield('property')
      
    <!-- ========== FOOTER ========== -->

    <hr class="my-0">

    <footer class="border-top">
      <div class="container">  
        <div class="col-xs-6 col-md-4 mt-4">
            <p class="small"><a href="https://www.instagram.com/iprosyar/" class="link-dark ">Instagram</a><p><span class="fa fab-instagram"></span>
        </div>

        <div class="text-center py-7">
          <!-- Copyright -->
          <p class="small text-muted mb-0">&copy; Cosan. 2019 ArifMrdn.</p>
          <!-- End Copyright -->
        </div>
      </div>
    </footer>
    
    <!-- ========== END FOOTER ========== -->

      <!-- Go to Top -->
      <a class="js-go-to u-go-to" href="#"
        data-position='{"bottom": 15, "right": 15 }'
        data-type="fixed"
        data-offset-top="400"
        data-compensation="#header"
        data-show-effect="slideInUp"
        data-hide-effect="slideOutDown">
        <span class="fas fa-arrow-up u-go-to__inner"></span>
      </a>
      <!-- End Go to Top -->

    <!-- JS Global Compulsory -->
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-migrate/dist/jquery-migrate.min.js') }}"></script>
    <script src="{{ asset('vendor/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ asset('vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
    <script src="{{ asset('vendor/svg-injector/dist/svg-injector.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('vendor/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ asset('vendor/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('vendor/fancybox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/slick-carousel/slick/slick.js') }}"></script>
    <script src="{{ asset('assets/vendor/dzsparallaxer/dzsparallaxer.js') }}"></script>

    <!-- JS Front -->
    <script src="{{ asset('assets/js/hs.core.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.header.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.unfold.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.focus-state.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.validation.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.selectpicker.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.range-slider.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.chartist-area-chart.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.fancybox.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.show-animation.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.cubeportfolio.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.svg-injector.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.go-to.js') }}"></script>
    <script src="{{ asset('assets/js/components/hs.slick-carousel.js') }}"></script>
    @stack('scripts')
    <!-- JS Plugins Init. -->
    <script>
      $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 767.98,
          hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
      });

      $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]:not(#filter3DropdownInvoker)'));

        $.HSCore.components.HSUnfold.init($('#filter3DropdownInvoker'), {
          afterOpen: function() {
            $.HSCore.components.HSRangeSlider.init('.js-range-slider');
            $.HSCore.components.HSChartistAreaChart.init('.js-area-chart');
          }
        });

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate', {
          rules: {
            confirmPassword: {
              equalTo: '#signupPassword'
            }
          }
        });

        // initialization of select picker
        $.HSCore.components.HSSelectPicker.init('.js-select');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of cubeportfolio
        $.HSCore.components.HSCubeportfolio.init('.cbp');

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
      });

      $(window).on('resize', function() {
        setTimeout(function() {
          $.HSCore.components.HSChartistAreaChart.init('.js-area-chart');
        }, 800);
      });
    </script>
  </body>
</html>
