@extends('layouts.property')

@section('property')
  <!-- ========== MAIN ========== -->
  <main id="content" class="body" role="main">
    <!-- Hero Section -->
      <div id="SVGwave1BottomSMShape" class="svg-preloader position-relative bg-light overflow-hidden">
        <div class="container space-top-2 space-bottom-3 space-top-md-5 space-top-lg-3">
          <div class="w-md-80 w-lg-60 text-center mx-auto">
            <h1 class="display-4 font-size-md-down-5 text-primary">Gallery<span class="font-weight-semi-bold"> PROSYAR</span></h1>
            <p class="lead"></p>
          </div>
        </div>

        <!-- SVG Background -->
        <figure class="position-absolute right-0 bottom-0 left-0">
          <img class="js-svg-injector" src="../../assets/svg/components/wave-1-bottom-sm.svg" alt="Image Description"
              data-parent="#SVGwave1BottomSMShape">
        </figure>
        <!-- End SVG Background Section -->
      </div>
      <!-- End Hero Section -->

      <!-- Portfolio Section -->
      <div class="container space-2 space-bottom-md-3">
        <div class="u-cubeportfolio space-bottom-2">

          <!-- Content -->
          <div class="cbp"
              data-layout="grid"
              data-controls="#filterControls"
              data-animation="quicksand"
              data-x-gap="32"
              data-y-gap="32"
              data-media-queries='[
                {"width": 1500, "cols": 4},
                {"width": 1100, "cols": 4},
                {"width": 800, "cols": 3},
                {"width": 480, "cols": 2},
                {"width": 300, "cols": 1}
              ]'>
           
          @foreach($galleries as $gallery)           
            @for ($i = 1; $i<=123; $i++)
              <div class="cbp-item rounded graphic">
                <a class="js-fancybox u-media-viewer cbp-caption" href="javascript:;"
                      data-src="{!! asset($gallery->gambar) !!}"
                      data-speed="700"
                      data-is-infinite="true">
                  <div class="cbp-caption-defaultWrap">
                    <img src="{!! asset($gallery->gambar) !!}" alt="Image Description">
                  </div>
                  <div class="cbp-caption-activeWrap bg-primary">
                    <div class="cbp-l-caption-alignCenter">
                      <div class="cbp-l-caption-body">
                        <h4 class="h6 text-white mb-0">Kepo</h4>
                        <p class="small text-white-70 mb-0">
                          by Tiberiu Neamu
                        </p>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            @endfor
          @endforeach

          </div>
          <!-- End Content -->
        </div>
      </div>
      <!-- End Portfolio Section -->

      <!-- Contact Us Section -->
      <div id="SVGwave1BottomSMShapeID2" class="svg-preloader">
        <div class="gradient-half-primary-v1">
          <div class="container space-top-2  space-bottom-2 space-top-md-1 space-bottom-md-3">
            <div class="row justify-content-md-between align-items-md-start">
              <div class="col-md-6 mb-7 mb-md-0">
                <h2 class="text-white mb-5">Cari Rumah?<br>Di <span class="font-weight-semi-bold">Prosyar </span>aja.</h2>
                <a class="h4 text-light" href="mailto:support@htmlstream.com">iprosyar@gmail.com</a>
              </div>

              <div class="col-md-6 mt-md-auto text-md-right">
                <!-- Social Networks -->
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="#">
                      <span class="fab fa-facebook-f btn-icon__inner"></span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="#">
                      <span class="fab fa-google btn-icon__inner"></span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="#">
                      <span class="fab fa-instagram btn-icon__inner"></span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="#">
                      <span class="fab fa-github btn-icon__inner"></span>
                    </a>
                  </li>
                </ul>
                <!-- End Social Networks -->

                <span class="h4 text-white-70"></span>
              </div>
            </div>
          </div>
        </div>

        <!-- SVG Background -->
        <figure class="position-absolute right-0 bottom-0 left-0 z-index-2">
          <img class="js-svg-injector" src="../../assets/svg/components/wave-1-bottom-sm.svg" alt="Image Description"
              data-parent="#SVGwave1BottomSMShapeID2">
        </figure>
        <!-- End SVG Background -->
      </div>
      <!-- End Contact Us Section -->
    </main>
    <!-- ========== END MAIN ========== -->
@endsection