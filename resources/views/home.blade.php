@extends('layouts.property')

@section('property')
  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" class="body" role="main">
    
    <!-- Hero Section -->
    
    <div class="d-md-flex bg-img-hero" style="background-image: url(../../assets/img/1920x1080/img32.jpg);">
      <div class="container d-md-flex align-items-md-center height-md-100vh position-relative space-top-2 space-bottom-3 space-top-md-3 space-top-lg-2 space-bottom-md-0">
        <div class="w-lg-65 mb-3">
          <div class="w-lg-65 w-xl-50 mb-5">
            <h1 class="display-4 text-white"><strong class="font-weight-semi-bold">Indonesia</strong> Property Syariah</h1>
          </div>
        </div>
      </div>
    </div>

    <!-- End Hero Section -->

    <!-- Icon Blocks Section -->
    <div class="bg-light">
        <div class="container space-2 space-md-3">
          
          <!-- Title -->
          
          <div class="w-md-60 w-lg-35 text-center mx-auto mb-9">
            <h2 class="h5 font-weight-normal">
               <span class="text-primary font-weight-semi-bold">Property Syariah</span>
            </h2>
          </div>
          
          <!-- End Title -->
  
          <div class="w-lg-85 mb-4 mx-lg-auto">
            <div class="card-deck d-block d-md-flex card-md-gutters-3">
          
              <!-- Card -->
              <article class="card text-center">
                <div class="card-body p-7">
          
                  <!-- SVG Icon -->
          
                  <div id="SVGforSale" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-for-sale">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/for-sale.svg" alt="Image Description"
                           data-parent="#SVGforSale">
                    </figure>
                  </div>
          
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Seller</h3>
                  <p class="mb-4">Discover how much property sold for with our comprehensive house price data.</p>
                  
                </div>
              </article>
          
              <!-- End Card -->
  
              <!-- Card -->
          
              <article class="card text-center">
                <div class="card-body p-7">
          
                  <!-- SVG Icon -->
          
                  <div id="SVGbuyer" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-buyer">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/buyer.svg" alt="Image Description"
                           data-parent="#SVGbuyer">
                    </figure>
                  </div>
          
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Buyer</h3>
                  <p class="mb-4">Ensure you find the right home, near the right school with new School Checker tool.</p>
                  
                </div>
              </article>
          
              <!-- End Card -->
          
            </div>
          </div>

          <div class="w-lg-85 mb-4 mx-lg-auto">
            <div class="card-deck d-block d-md-flex card-md-gutters-3">
          
              <!-- Card -->
          
              <article class="card text-center">
                <div class="card-body p-7">
          
                  <!-- SVG Icon -->
          
                  <div id="SVGforSale" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-for-sale">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/for-sale.svg" alt="Image Description"
                            data-parent="#SVGforSale">
                    </figure>
                  </div>
          
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Seller</h3>
                  <p class="mb-4">Discover how much property sold for with our comprehensive house price data.</p>
                  
                </div>
              </article>
          
              <!-- End Card -->
  
              <!-- Card -->
          
              <article class="card text-center">
                <div class="card-body p-7">
          
                  <!-- SVG Icon -->
                  <div id="SVGbuyer" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-buyer">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/buyer.svg" alt="Image Description"
                            data-parent="#SVGbuyer">
                    </figure>
                  </div>
          
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Buyer</h3>
                  <p class="mb-4">Ensure you find the right home, near the right school with new School Checker tool.</p>
                  
                </div>
              </article>
          
              <!-- End Card -->
          
            </div>
          </div>
          
          <div class="w-lg-85 mb-4 mx-lg-auto">
            <div class="card-deck d-block d-md-flex card-md-gutters-3">
              
              <!-- Card -->
              
              <article class="card text-center">
                <div class="card-body p-7">
              
                  <!-- SVG Icon -->
              
                  <div id="SVGforSale" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-for-sale">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/for-sale.svg" alt="Image Description"
                            data-parent="#SVGforSale">
                    </figure>
                  </div>
              
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Seller</h3>
                  <p class="mb-4">Discover how much property sold for with our comprehensive house price data.</p>
                  
                </div>
              </article>
              
              <!-- End Card -->
  
              <!-- Card -->
              
              <article class="card text-center">
                <div class="card-body p-7">
              
                  <!-- SVG Icon -->
              
                  <div id="SVGbuyer" class="svg-preloader w-75 mx-auto mb-6">
                    <figure class="ie-buyer">
                      <img class="js-svg-injector" src="../../assets/svg/flat-icons/buyer.svg" alt="Image Description"
                            data-parent="#SVGbuyer">
                    </figure>
                  </div>
              
                  <!-- End SVG Icon -->
  
                  <h3 class="h4 mb-3">I'm a Buyer</h3>
                  <p class="mb-4">Ensure you find the right home, near the right school with new School Checker tool.</p>
                  
                </div>
              </article>
              
              <!-- End Card -->
            </div>
          </div>
        </div>
      </div>

      <!-- End Icon Blocks Section -->

    <!-- Houses Section -->
    <div class="bg-muted">
      <div class="container space-1 space-md-2">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <figure id="icon13" class="svg-preloader ie-height-72 max-width-10 mx-auto mb-3">
            <img class="js-svg-injector" src="../../assets/svg/icons/icon-13.svg" alt="SVG"
                 data-parent="#icon13">
          </figure>
          <h2 class="font-weight-medium">Silahkan temukan rumah sesuai kriteria anda.</h2>
        </div>
        <!-- End Title -->

        <!-- Slick Carousel -->
        <div class="js-slick-carousel u-slick u-slick-zoom u-slick--gutters-3 mb-7"
             data-slides-show="3"
             data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
             data-responsive='[{
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 1
               }
             }]'>
          @foreach($houses as $house)
            <div class="js-slide card border-0 shadow-sm mb-3">
              <!-- House Items -->
              <div class="position-relative">
                <img class="card-img-top" src="{{Helper::checkImagePath(json_decode($house->gambar)[0])}}" alt="Image Description">
                <header class="position-absolute top-0 right-0 left-0 p-5">
                  <a class="media align-items-center text-white" href="#">
                    <div class="u-avatar mr-2">
                      <img class="img-fluid rounded-circle" src="{{ Helper::checkProfileImg($house->contact->gambar) }}" width="60px" height="60px" alt="Image Description">
                    </div>
                    <div class="media-body">
                      <span>{{$house->contact->nama}}</span>
                    </div>
                  </a>
                </header>
                <div class="position-absolute right-0 bottom-0 left-0 p-5">
                  <span class="h4 text-white">{{$house->harga}}</span>
                </div>
              </div>

              <div class="card-body p-5">
                <span class="fas fa-map-marker-alt text-danger mr-2"></span>
                <a class="text-secondary" href="#">{{str_limit($house->alamat, 50, ' ..')}}</a>
              </div>
              <!-- End House Items -->
            </div>
          @endforeach
        </div>
        <!-- End Slick Carousel -->

        <div class="text-center">
          <a class="btn btn-sm btn-soft-primary btn-wide transition-3d-hover" href="{{ url('/property-grid') }}">Tampilkan Semua Property</a>
        </div>
      </div>
    </div>
    <!-- End Houses Section -->
  </main>
  <!-- ========== END MAIN CONTENT ========== -->
@endsection
