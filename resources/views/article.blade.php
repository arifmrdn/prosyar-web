@extends('layouts.property')

@section('property')

    <!-- ========== MAIN ========== -->
    <main id="content" role="main">
        
        <!-- Blog Grid Section -->
        
        <div class="container space-2 space-top-md-5 space-top-lg-4">
        
            <!-- Title -->
        
            <div class="w-md-80 w-lg-60 mb-9">
                <h1 class="font-weight-normal">Blog grid - <span class="text-primary font-weight-semi-bold">Full Width</span></h1>          
            </div>
          
            <!-- End Title -->
    
            <div class="card-deck d-block d-md-flex card-md-gutters-2">
             
                <!-- Blog Grid -->
                
                <article class="card border-0 shadow-sm mb-3">
                    
                    <div class="card-body p-5">
                        <small class="d-block text-muted mb-2">May 15, 2018</small>
                        <h2 class="h5">
                            <a href="single-article-classic.html">InVision design forward fund</a>
                        </h2>
                        <p class="mb-0">Clark Valberg is the founder and CEO of InVision.</p>
                    </div>
    
                    <div class="card-footer pb-5 px-0 mx-5">
                        <div class="media align-items-center">
                            <div class="u-sm-avatar mr-3">
                                <img class="img-fluid rounded-circle" src="../../assets/img/100x100/img1.jpg" alt="Image Description">
                            </div>
                            <div class="media-body">
                                <h4 class="small mb-0"><a href="single-article-classic.html">Andrea Gard</a></h4>
                            </div>
                        </div>
                    </div>
                
                </article>

                <!-- End Blog Grid -->
    
                <!-- Blog Grid -->
                
                <article class="card border-0 shadow-sm mb-3">
                
                    <div class="card-body p-5">
                        <small class="d-block text-muted mb-2">May 22, 2018</small>
                        <h3 class="h5">
                            <a href="single-article-classic.html">Announcing a plan for small teams</a>
                        </h3>
                        <p class="mb-0">We've always believed that by providing a space</p>
                    </div>  
        
                    <div class="card-footer pb-5 px-0 mx-5">
                        <div class="media align-items-center">
                            <div class="u-sm-avatar mr-3">
                                <img class="img-fluid rounded-circle" src="../../assets/img/100x100/img3.jpg" alt="Image Description">
                            </div>
                            <div class="media-body">
                                <h4 class="small mb-0"><a href="single-article-classic.html">James Austin</a></h4>
                            </div>
                        </article>
                        </div>
                    </div>
                <!-- End Blog Grid -->
        
                <!-- Blog Grid -->
                <article class="card border-0 shadow-sm mb-3">
                    <div     class="card-body p-5">
                        <small class="d-block text-muted mb-2">May 30, 2018</small>
                        <h3 class="h5">
                            <a href="single-article-classic.html">Design principles</a>
                        </h3>
                        <p class="mb-0">The biggest collections of design principles on the internet</p>
                    </div>
        
                    <div class="card-footer pb-5 px-0 mx-5">
                        <div class="media align-items-center">
                            <div class="u-sm-avatar mr-3">
                                <img class="img-fluid rounded-circle" src="../../assets/img/100x100/img2.jpg" alt="Image Description">
                            </div>
                            <div class="media-body">
                            <h4 class="small mb-0"><a href="single-article-classic.html">Charlotte Moore</a></h4>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Blog Grid -->
          


        <!-- SVG Background -->
        
        <figure class="position-absolute right-0 bottom-0 left-0 z-index-2">
            <img class="js-svg-injector" src="../../assets/svg/components/wave-1-bottom-sm.svg" alt="Image Description"
                data-parent="#SVGsubscribe">
        </figure>
        
        <!-- End SVG Background Section -->
        
        </div>
        
        <!-- End Subscribe Section -->
    
    </main>
    <!-- ========== END MAIN ========== -->

@endsection