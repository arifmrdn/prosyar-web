<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <form method='post' enctype="multipart/form-data" action='{{route('property.store')}}'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class='panel-heading'>Add Form</div>
        <div class='panel-body'>

            <div class='form-group'>
            <label>Nomor Property</label>
            <input type='text' name='id_property' required class='form-control'/>
            </div>

            <div class='form-group'>
                <label>Tipe</label>
                <select name="tipe" class="form-control" required>
                    <option value="" selected disabled>-- Pilih Tipe --</option>
                    <option value="Rumah">Rumah</option>
                    <option value="Flat">Flat</option>
                    <option value="Multi Keluarga">Multi Keluarga</option>
                </select>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class='form-group'>
                        <label>Jumlah Dapur</label>
                        <input type='number' name='dapur' required class='form-control'/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class='form-group'>
                        <label>Jumlah Kamar Mandi</label>
                        <input type='number' name='kamar_mandi' required class='form-control'/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class='form-group'>
                        <label>Jumlah Kamar Tidur</label>
                        <input type='number' name='kamar_tidur' required class='form-control'/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class='form-group'>
                        <label>Ruang Tamu</label>
                        <input type='number' name='ruang_tamu' required class='form-control'/>
                    </div>
                </div>
            </div>

            <div class='form-group'>
                <label>Denah</label>
                <input type='file' name='denah' class='form-control'/>
            </div>

            <div class='form-group'>
                <label>Gambar</label>
                <input name="gambar[]" type="file" multiple class="form-control"/>
            </div>

            <div class='form-group'>
                <label>Lokasi</label>
                <select name="locations_id" class="form-control" required>
                    <option value="">-- Pilih Lokasi --</option>
                    @foreach ($locations as $location)
                        <option value="{{$location->id}}">{{$location->nama}}</option>
                    @endforeach
                </select>
            </div>

            <div class='form-group'>
                <label>Nama / Judul</label>
                <input type='text' name='nama' required class='form-control'/>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Lebar</label>
                        <input type='number' name='lebar' required class='form-control'/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Panjang</label>
                        <input type='number' name='panjang' required class='form-control'/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Listrik</label>
                        <input type='number' name='listrik' required class='form-control'/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Sertifikat</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sertifikat" id="sertifikat1" value="1" required>
                            <label class="form-check-label" for="sertifikat1">Ada</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sertifikat" id="sertifikat2" value="0" required>
                            <label class="form-check-label" for="sertifikat2">Tidak Ada</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Tempat Parkir</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tempat_parkir" id="tempat_parkir1" value="1" required>
                            <label class="form-check-label" for="tempat_parkir1">Ada</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tempat_parkir" id="tempat_parkir2" value="0" required>
                            <label class="form-check-label" for="tempat_parkir2">Tidak Ada</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class='form-group'>
                        <label>Sudah Ditempati</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sudah_ditempati" id="sudah_ditempati1" value="1" required>
                            <label class="form-check-label" for="sudah_ditempati1">Ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sudah_ditempati" id="sudah_ditempati2" value="0" required>
                            <label class="form-check-label" for="sudah_ditempati2">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class='form-group'>
                <label>Alamat</label>
                <textarea name="alamat" class="form-control" required></textarea>
            </div>

            <div class='form-group'>
                <label>Deskripsi</label>
                <textarea name="deskripsi" class="form-control" required></textarea>
            </div>

            <div class='form-group'>
                <label>Terdaftar</label>
                <input type='date' name='terdaftar' required class='form-control'/>
            </div>

            <div class='form-group'>
                <label>Harga</label>
                <input type='number' name='harga' required class='form-control'/>
            </div>

            <div class='form-group'>
                <label>Kontak</label>
                <select name="contacts_id" class="form-control" required>
                    <option value="" disabled selected>-- Pilih Kontak --</option>
                    @foreach ($contacts as $contact)
                        <option value="{{$contact->id}}">{{$contact->nama}}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class='panel-footer'>
        <input type='submit' class='btn btn-primary' value='Save changes'/>
        </div>

    </form>
  </div>
@endsection