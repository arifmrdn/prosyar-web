@extends('layouts.property')

@section('property-description')
  
  @foreach($contacts as $contact)

  <!-- ========== MAIN ========== -->
  <main id="content" role="main"> 
    <!-- Hero Section -->
    <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll"
          data-options='{direction: "normal"}'>
      <!-- Apply your Parallax background image here -->
      <div class="divimage dzsparallaxer--target" style="height: 120%; background-color: black;  background-image: url('{{ asset('assets/contact/img13.jpg') }}');"></div>
      <!-- Content -->
      <div class="container position-relative space-2 space-top-md-5 space-bottom-md-3 z-index-2">
        <div class="w-lg-80 text-center mx-auto">
          <h1 class="display-3 font-size-md-down-5 text-white font-weight-semi-bold" style="font-size: 50px; font-family: poppins;">Indonesia Property Syariah</h1>
          <div class="media">
            <div class="media-body">
                <h4 class="h6 mb-1">{!! $houses->$house !!}</h4>
                <a class="btn btn-md btn-primary" href="https://wa.me/6281315511500">Hubungi Saya</a>
            </div>
          </div>
        </div>
      </div>
      <!-- End Content -->
    </div>
    <!-- End Hero Section -->
          
    <!-- Contacts Info Section -->
    <div class="clearfix space-2">
      <div class="row no-gutters">
        <div class="col-sm-6 col-lg-3 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon8" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-8.svg" alt="SVG"
                    data-parent="#icon8">
            </figure>
            <h2 class="h6 mb-0">Alamat</h2>
            <p class="mb-0">{!! $contact->alamat !!}</p>
          </div>
          <!-- End Contacts Info -->
        </div>

        <div class="col-sm-6 col-lg-3 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon15" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-15.svg" alt="SVG"
                    data-parent="#icon15">
            </figure>
            <h3 class="h6 mb-0">Email</h3>
            <p class="mb-0">{!! $contact->email !!}</p>
          </div>
          <!-- End Contacts Info -->
        </div>
    
        <div class="col-sm-6 col-lg-3 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon16" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-16.svg" alt="SVG"
                    data-parent="#icon16">
            </figure>
            <h3 class="h6 mb-0">Nomor Ponsel</h3>
            <p class="mb-0">{!! $contact->nomor_telepon !!}</p>
          </div>
          <!-- End Contacts Info -->
        </div>

        <div class="col-sm-6 col-lg-3">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon17" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-17.svg" alt="SVG"
                    data-parent="#icon17">
            </figure>
            <h3 class="h6 mb-0">Telepon Rumah</h3>
            <p class="mb-0">+1 (062) 109-9223</p>
          </div>
          <!-- End Contacts Info -->
        </div>
      </div>
    </div>
    <!-- End Contacts Info Section -->

    <hr class="my-0">
        
  </main>
  
  @endforeach
      <!-- ========== END MAIN ========== -->
@endsection