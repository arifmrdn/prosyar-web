-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: property
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_apicustom`
--

DROP TABLE IF EXISTS `cms_apicustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_apicustom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `responses` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apicustom`
--

LOCK TABLES `cms_apicustom` WRITE;
/*!40000 ALTER TABLE `cms_apicustom` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apicustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_apikey`
--

DROP TABLE IF EXISTS `cms_apikey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_apikey` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `screetkey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apikey`
--

LOCK TABLES `cms_apikey` WRITE;
/*!40000 ALTER TABLE `cms_apikey` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apikey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_dashboard`
--

DROP TABLE IF EXISTS `cms_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_dashboard`
--

LOCK TABLES `cms_dashboard` WRITE;
/*!40000 ALTER TABLE `cms_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_queues`
--

DROP TABLE IF EXISTS `cms_email_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_email_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `email_attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_queues`
--

LOCK TABLES `cms_email_queues` WRITE;
/*!40000 ALTER TABLE `cms_email_queues` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_email_queues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_templates`
--

DROP TABLE IF EXISTS `cms_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_templates`
--

LOCK TABLES `cms_email_templates` WRITE;
/*!40000 ALTER TABLE `cms_email_templates` DISABLE KEYS */;
INSERT INTO `cms_email_templates` VALUES (1,'Email Template Forgot Password Backend','forgot_password_backend',NULL,'<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>','[password]','System','system@crudbooster.com',NULL,'2019-07-07 11:04:28',NULL),(2,'Email Template Forgot Password Backend','forgot_password_backend',NULL,'<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>','[password]','System','system@crudbooster.com',NULL,'2019-07-07 11:04:48',NULL),(3,'Email Template Forgot Password Backend','forgot_password_backend',NULL,'<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>','[password]','System','system@crudbooster.com',NULL,'2019-07-20 03:32:25',NULL);
/*!40000 ALTER TABLE `cms_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_logs`
--

DROP TABLE IF EXISTS `cms_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_logs`
--

LOCK TABLES `cms_logs` WRITE;
/*!40000 ALTER TABLE `cms_logs` DISABLE KEYS */;
INSERT INTO `cms_logs` VALUES (1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2019-07-07 11:05:12',NULL),(2,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/users/edit-save/1','Update data Super Admin at Users Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2019-07/joyful_woman_holding_cellphone_gesturing_thumbup_against_yellow_surface_23_2148188743.jpg</td></tr><tr><td>email</td><td>admin@crudbooster.com</td><td>admin@prosyar.com</td></tr><tr><td>password</td><td>$2y$10$ihqQmSb19UH0/e7Qac6Er.hqpGlonbGmO2WbV3nrKvAvVE3khyjjW</td><td>$2y$10$K1DRATLxF1Mjjq3gihjYO.YxGVJNOPISbfIT/VbpNRo6rg87ETE5S</td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>',1,'2019-07-07 11:05:53',NULL),(3,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/logout','admin@prosyar.com logout','',1,'2019-07-07 11:06:01',NULL),(4,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-07 11:08:27',NULL),(5,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Jakarta at Lokasi','',1,'2019-07-07 11:21:09',NULL),(6,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Tangerang Selatan at Lokasi','',1,'2019-07-07 11:21:13',NULL),(7,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Parung at Lokasi','',1,'2019-07-07 11:21:16',NULL),(8,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Depok at Lokasi','',1,'2019-07-07 11:21:18',NULL),(9,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Bogor at Lokasi','',1,'2019-07-07 11:21:21',NULL),(10,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Bandung at Lokasi','',1,'2019-07-07 11:21:27',NULL),(11,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data P10002A at Property','',1,'2019-07-07 11:26:20',NULL),(12,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data P10002V at Property','',1,'2019-07-07 11:27:01',NULL),(13,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/houses/edit-save/2','Update data P10002V at Property','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2019-07-07 11:35:37',NULL),(14,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/houses/edit-save/2','Update data P10002V at Property','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>panjang</td><td>100</td><td>150</td></tr><tr><td>lebar</td><td>100</td><td>88</td></tr></tbody></table>',1,'2019-07-07 11:37:11',NULL),(15,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/logout','admin@prosyar.com logout','',1,'2019-07-07 12:53:19',NULL),(16,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-07 12:53:25',NULL),(17,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/logout','admin@prosyar.com logout','',1,'2019-07-07 13:06:24',NULL),(18,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-07 13:06:30',NULL),(19,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-08 11:15:29',NULL),(20,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/contacs/add-save','Add New Data  at Kontak','',1,'2019-07-08 11:19:47',NULL),(21,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-09 03:16:58',NULL),(22,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/delete/3','Delete data Kontak at Menu Management','',1,'2019-07-09 03:22:20',NULL),(23,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Kontak at Menu Management','',1,'2019-07-09 03:25:16',NULL),(24,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/edit-save/4','Update data Kontak at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td></td><td>AdminContactsControllerGetIndex</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>',1,'2019-07-09 03:32:24',NULL),(25,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/delete/4','Delete data Kontak at Menu Management','',1,'2019-07-09 03:32:54',NULL),(26,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Kontak at Menu Management','',1,'2019-07-09 03:33:49',NULL),(27,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/edit-save/5','Update data Kontak at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td></td><td></td></tr></tbody></table>',1,'2019-07-09 03:45:45',NULL),(28,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/module_generator/delete/14','Delete data Kontak at Module Generator','',1,'2019-07-09 03:47:16',NULL),(29,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/contacts/add-save','Add New Data  at Kontak','',1,'2019-07-09 03:54:26',NULL),(30,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/contacts/delete/1','Delete data  at Kontak','',1,'2019-07-09 04:24:12',NULL),(31,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/contacts/add-save','Add New Data  at Kontak','',1,'2019-07-09 04:26:10',NULL),(32,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-10 00:41:19',NULL),(33,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/delete/6','Delete data Kontak at Menu Management','',1,'2019-07-10 00:43:57',NULL),(34,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Galeri at Menu Management','',1,'2019-07-10 01:03:08',NULL),(35,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/delete/8','Delete data Galeri at Menu Management','',1,'2019-07-10 01:08:25',NULL),(36,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/delete/7','Delete data Galeri at Menu Management','',1,'2019-07-10 01:36:11',NULL),(37,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-14 05:59:53',NULL),(38,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Galeri at Menu Management','',1,'2019-07-14 06:02:29',NULL),(39,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/module_generator/delete/16','Delete data Galeri at Module Generator','',1,'2019-07-14 06:05:19',NULL),(40,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/module_generator/delete/17','Delete data Galeri at Module Generator','',1,'2019-07-14 06:09:22',NULL),(41,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/galleries/add-save','Add New Data  at Galeri','',1,'2019-07-14 06:20:10',NULL),(42,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/galleries/add-save','Add New Data  at Galeri','',1,'2019-07-14 06:20:54',NULL),(43,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/galleries/add-save','Add New Data  at Galeri','',1,'2019-07-14 06:21:41',NULL),(44,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-14 16:21:44',NULL),(45,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/module_generator/delete/19','Delete data Artikel at Module Generator','',1,'2019-07-14 16:29:14',NULL),(46,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36','http://localhost:8000/admin/menu_management/edit-save/10','Update data Artikel at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-book</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>',1,'2019-07-14 16:33:13',NULL),(47,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-18 10:26:49',NULL),(48,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-18 13:11:23',NULL),(49,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/6','Delete data Bandung at Lokasi','',1,'2019-07-18 13:18:30',NULL),(50,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/5','Delete data Bogor at Lokasi','',1,'2019-07-18 13:18:49',NULL),(51,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/4','Delete data Depok at Lokasi','',1,'2019-07-18 13:19:05',NULL),(52,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/4','Delete data  at Lokasi','',1,'2019-07-18 13:19:06',NULL),(53,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/3','Delete data Parung at Lokasi','',1,'2019-07-18 13:19:23',NULL),(54,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/1','Delete data Jakarta at Lokasi','',1,'2019-07-18 13:19:41',NULL),(55,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/delete/2','Delete data Tangerang Selatan at Lokasi','',1,'2019-07-18 13:19:57',NULL),(56,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/galleries/edit-save/3','Update data  at Galeri','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2019-07-18 13:44:14',NULL),(57,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-20 01:42:46',NULL),(58,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/users/add-save','Add New Data Admin at Users Management','',1,'2019-07-20 01:45:20',NULL),(59,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/login','iprosyar@gmail.com login with IP Address 127.0.0.1','',2,'2019-07-20 03:25:53',NULL),(60,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/module_generator/delete/20','Delete data Artikel at Module Generator','',2,'2019-07-20 03:34:48',NULL),(61,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/login','admin@prosyar.com login with IP Address 127.0.0.1','',1,'2019-07-20 03:49:31',NULL),(62,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Jakarta at Lokasi','',1,'2019-07-20 04:41:20',NULL),(63,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data awdadw at Property','',1,'2019-07-20 05:06:05',NULL),(64,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data 1adawdawd121 at Property','',1,'2019-07-20 05:07:56',NULL),(65,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data 12eadawd1 at Property','',1,'2019-07-20 05:10:43',NULL),(66,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data a12ad at Property','',1,'2019-07-20 05:12:51',NULL),(67,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/add-save','Add New Data awdadwad at Property','',1,'2019-07-20 05:13:39',NULL),(68,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/locations/add-save','Add New Data Tangerang Selatan at Lokasi','',1,'2019-07-20 07:18:45',NULL),(69,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/34','Delete data adadadwad at Property','',1,'2019-07-20 08:25:05',NULL),(70,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/33','Delete data adadadwad at Property','',1,'2019-07-20 08:25:09',NULL),(71,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/32','Delete data adadadwad at Property','',1,'2019-07-20 08:25:13',NULL),(72,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/31','Delete data adadadwad at Property','',1,'2019-07-20 08:25:19',NULL),(73,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/30','Delete data adadadwad at Property','',1,'2019-07-20 08:26:15',NULL),(74,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/houses/delete/35','Delete data adadadwad at Property','',1,'2019-07-20 08:27:24',NULL),(75,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/contacts/delete-image','Delete the image of  at Kontak','',1,'2019-07-20 09:00:47',NULL),(76,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/contacts/edit-save/2','Update data  at Kontak','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>gambar</td><td></td><td>uploads/1/2019-07/img9.jpg</td></tr></tbody></table>',1,'2019-07-20 09:00:53',NULL),(77,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','http://localhost:8000/admin/contacts/delete-image','Delete the image of  at Kontak','',1,'2019-07-20 09:04:25',NULL);
/*!40000 ALTER TABLE `cms_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus`
--

DROP TABLE IF EXISTS `cms_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus`
--

LOCK TABLES `cms_menus` WRITE;
/*!40000 ALTER TABLE `cms_menus` DISABLE KEYS */;
INSERT INTO `cms_menus` VALUES (1,'Property','Route','AdminHousesControllerGetIndex',NULL,'fa fa-home',0,1,0,1,1,'2019-07-07 11:11:07',NULL),(2,'Lokasi','Route','AdminLocationsControllerGetIndex',NULL,'fa fa-map-pin',0,1,0,1,2,'2019-07-07 11:19:18',NULL),(5,'Kontak','Route','AdminContactsControllerGetIndex','normal','fa fa-phone',0,1,0,1,5,'2019-07-09 03:33:49','2019-07-09 03:45:44'),(8,'Galeri','Route','AdminGalleriesControllerGetIndex',NULL,'fa fa-glass',0,1,0,1,3,'2019-07-14 06:15:22',NULL),(11,'articles','Route','AdminArticlesControllerGetIndex',NULL,'fa fa-glass',0,1,0,1,6,'2019-07-20 03:35:54',NULL);
/*!40000 ALTER TABLE `cms_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus_privileges`
--

DROP TABLE IF EXISTS `cms_menus_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_menus_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus_privileges`
--

LOCK TABLES `cms_menus_privileges` WRITE;
/*!40000 ALTER TABLE `cms_menus_privileges` DISABLE KEYS */;
INSERT INTO `cms_menus_privileges` VALUES (1,1,1),(2,2,1),(3,3,1),(5,4,1),(7,5,1),(9,7,1),(10,8,1),(11,6,1),(12,7,1),(13,8,1),(14,9,1),(16,10,1),(17,11,1);
/*!40000 ALTER TABLE `cms_menus_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_moduls`
--

DROP TABLE IF EXISTS `cms_moduls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_moduls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_moduls`
--

LOCK TABLES `cms_moduls` WRITE;
/*!40000 ALTER TABLE `cms_moduls` DISABLE KEYS */;
INSERT INTO `cms_moduls` VALUES (1,'Notifications','fa fa-cog','notifications','cms_notifications','NotificationsController',1,1,'2019-07-07 11:04:28',NULL,NULL),(2,'Privileges','fa fa-cog','privileges','cms_privileges','PrivilegesController',1,1,'2019-07-07 11:04:28',NULL,NULL),(3,'Privileges Roles','fa fa-cog','privileges_roles','cms_privileges_roles','PrivilegesRolesController',1,1,'2019-07-07 11:04:28',NULL,NULL),(4,'Users Management','fa fa-users','users','cms_users','AdminCmsUsersController',0,1,'2019-07-07 11:04:28',NULL,NULL),(5,'Settings','fa fa-cog','settings','cms_settings','SettingsController',1,1,'2019-07-07 11:04:28',NULL,NULL),(6,'Module Generator','fa fa-database','module_generator','cms_moduls','ModulsController',1,1,'2019-07-07 11:04:28',NULL,NULL),(7,'Menu Management','fa fa-bars','menu_management','cms_menus','MenusController',1,1,'2019-07-07 11:04:28',NULL,NULL),(8,'Email Templates','fa fa-envelope-o','email_templates','cms_email_templates','EmailTemplatesController',1,1,'2019-07-07 11:04:28',NULL,NULL),(9,'Statistic Builder','fa fa-dashboard','statistic_builder','cms_statistics','StatisticBuilderController',1,1,'2019-07-07 11:04:28',NULL,NULL),(10,'API Generator','fa fa-cloud-download','api_generator','','ApiCustomController',1,1,'2019-07-07 11:04:28',NULL,NULL),(11,'Log User Access','fa fa-flag-o','logs','cms_logs','LogsController',1,1,'2019-07-07 11:04:28',NULL,NULL),(12,'Property','fa fa-home','houses','houses','AdminHousesController',0,0,'2019-07-07 11:11:07',NULL,NULL),(13,'Lokasi','fa fa-map-pin','locations','locations','AdminLocationsController',0,0,'2019-07-07 11:19:18',NULL,NULL),(14,'Kontak','fa fa-glass','contacs','contacs','AdminContacsController',0,0,'2019-07-07 12:09:58',NULL,'2019-07-09 03:47:17'),(15,'Kontak','fa fa-phone','contacts','contacts','AdminContactsController',0,0,'2019-07-09 03:49:44',NULL,NULL),(16,'Galeri','fa fa-camera','galleries','galleries','AdminGalleriesController',0,0,'2019-07-10 00:49:34',NULL,'2019-07-14 06:05:20'),(17,'Galeri','fa fa-glass','AdminGalleriesController','galleries','AdminAdminGalleriesControllerController',0,0,'2019-07-14 06:06:48',NULL,'2019-07-14 06:09:22'),(18,'Galeri','fa fa-glass','galleries','galleries','AdminGalleriesController',0,0,'2019-07-14 06:15:22',NULL,NULL),(19,'Artikel','fa fa-glass','articles','articles','AdminArticlesController',0,0,'2019-07-14 16:23:47',NULL,'2019-07-14 16:29:15'),(20,'Artikel','fa fa-glass','articles','articles','AdminArticlesController',0,0,'2019-07-14 16:29:53',NULL,'2019-07-20 03:34:48'),(21,'articles','fa fa-glass','articles','articles','AdminArticlesController',0,0,'2019-07-20 03:35:53',NULL,NULL);
/*!40000 ALTER TABLE `cms_moduls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_notifications`
--

DROP TABLE IF EXISTS `cms_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_notifications`
--

LOCK TABLES `cms_notifications` WRITE;
/*!40000 ALTER TABLE `cms_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges`
--

DROP TABLE IF EXISTS `cms_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges`
--

LOCK TABLES `cms_privileges` WRITE;
/*!40000 ALTER TABLE `cms_privileges` DISABLE KEYS */;
INSERT INTO `cms_privileges` VALUES (1,'Super Administrator',1,'skin-red','2019-07-07 11:04:28',NULL);
/*!40000 ALTER TABLE `cms_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges_roles`
--

DROP TABLE IF EXISTS `cms_privileges_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_privileges_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges_roles`
--

LOCK TABLES `cms_privileges_roles` WRITE;
/*!40000 ALTER TABLE `cms_privileges_roles` DISABLE KEYS */;
INSERT INTO `cms_privileges_roles` VALUES (1,1,0,0,0,0,1,1,'2019-07-07 11:04:28',NULL),(2,1,1,1,1,1,1,2,'2019-07-07 11:04:28',NULL),(3,0,1,1,1,1,1,3,'2019-07-07 11:04:28',NULL),(4,1,1,1,1,1,1,4,'2019-07-07 11:04:28',NULL),(5,1,1,1,1,1,1,5,'2019-07-07 11:04:28',NULL),(6,1,1,1,1,1,1,6,'2019-07-07 11:04:28',NULL),(7,1,1,1,1,1,1,7,'2019-07-07 11:04:28',NULL),(8,1,1,1,1,1,1,8,'2019-07-07 11:04:28',NULL),(9,1,1,1,1,1,1,9,'2019-07-07 11:04:28',NULL),(10,1,1,1,1,1,1,10,'2019-07-07 11:04:28',NULL),(11,1,0,1,0,1,1,11,'2019-07-07 11:04:28',NULL),(12,1,1,1,1,1,1,12,NULL,NULL),(13,1,1,1,1,1,1,13,NULL,NULL),(14,1,1,1,1,1,1,14,NULL,NULL),(15,1,1,1,1,1,1,15,NULL,NULL),(16,1,1,1,1,1,1,16,NULL,NULL),(17,1,1,1,1,1,1,17,NULL,NULL),(18,1,1,1,1,1,1,18,NULL,NULL),(19,1,1,1,1,1,1,19,NULL,NULL),(20,1,1,1,1,1,1,20,NULL,NULL),(21,1,1,1,1,1,1,21,NULL,NULL);
/*!40000 ALTER TABLE `cms_privileges_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_settings`
--

DROP TABLE IF EXISTS `cms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_settings`
--

LOCK TABLES `cms_settings` WRITE;
/*!40000 ALTER TABLE `cms_settings` DISABLE KEYS */;
INSERT INTO `cms_settings` VALUES (1,'login_background_color',NULL,'text',NULL,'Input hexacode','2019-07-07 11:04:28',NULL,'Login Register Style','Login Background Color'),(2,'login_font_color',NULL,'text',NULL,'Input hexacode','2019-07-07 11:04:28',NULL,'Login Register Style','Login Font Color'),(3,'login_background_image',NULL,'upload_image',NULL,NULL,'2019-07-07 11:04:28',NULL,'Login Register Style','Login Background Image'),(4,'email_sender','support@crudbooster.com','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Email Setting','Email Sender'),(5,'smtp_driver','mail','select','smtp,mail,sendmail',NULL,'2019-07-07 11:04:28',NULL,'Email Setting','Mail Driver'),(6,'smtp_host','','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Email Setting','SMTP Host'),(7,'smtp_port','25','text',NULL,'default 25','2019-07-07 11:04:28',NULL,'Email Setting','SMTP Port'),(8,'smtp_username','','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Email Setting','SMTP Username'),(9,'smtp_password','','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Email Setting','SMTP Password'),(10,'appname','CRUDBooster','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Application Setting','Application Name'),(11,'default_paper_size','Legal','text',NULL,'Paper size, ex : A4, Legal, etc','2019-07-07 11:04:28',NULL,'Application Setting','Default Paper Print Size'),(12,'logo','','upload_image',NULL,NULL,'2019-07-07 11:04:28',NULL,'Application Setting','Logo'),(13,'favicon','','upload_image',NULL,NULL,'2019-07-07 11:04:28',NULL,'Application Setting','Favicon'),(14,'api_debug_mode','true','select','true,false',NULL,'2019-07-07 11:04:28',NULL,'Application Setting','API Debug Mode'),(15,'google_api_key','','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Application Setting','Google API Key'),(16,'google_fcm_key','','text',NULL,NULL,'2019-07-07 11:04:28',NULL,'Application Setting','Google FCM Key');
/*!40000 ALTER TABLE `cms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistic_components`
--

DROP TABLE IF EXISTS `cms_statistic_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_statistic_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistic_components`
--

LOCK TABLES `cms_statistic_components` WRITE;
/*!40000 ALTER TABLE `cms_statistic_components` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_statistic_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistics`
--

DROP TABLE IF EXISTS `cms_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistics`
--

LOCK TABLES `cms_statistics` WRITE;
/*!40000 ALTER TABLE `cms_statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users`
--

LOCK TABLES `cms_users` WRITE;
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` VALUES (1,'Super Admin','uploads/1/2019-07/joyful_woman_holding_cellphone_gesturing_thumbup_against_yellow_surface_23_2148188743.jpg','admin@prosyar.com','$2y$10$K1DRATLxF1Mjjq3gihjYO.YxGVJNOPISbfIT/VbpNRo6rg87ETE5S',1,'2019-07-07 11:04:28',NULL,'Active'),(2,'Admin','uploads/1/2019-07/img22.jpg','iprosyar@gmail.com','$2y$10$KufwER1jZu1uPszigMJyhebDxCKXlBlZMMukxJfGDCXVh96O.Zctq',1,'2019-07-20 01:45:20',NULL,NULL);
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_telepon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (2,'Angga ',NULL,'iprosyar@gmail.com','Jl.Melon','081315511500');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Ijab deal rumah pondok pinang','uploads/1/2019-07/img1.jpg','Berhasil menjual rumah'),(2,'Ijab deal rumah pondok kacang','uploads/1/2019-07/img2.jpg','Berhasil menjual rumah'),(3,'Ijab deal rumah DKI Jakarta','uploads/1/2019-07/img3.jpg','Berhasil menjual rumah');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `homes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homes`
--

LOCK TABLES `homes` WRITE;
/*!40000 ALTER TABLE `homes` DISABLE KEYS */;
/*!40000 ALTER TABLE `homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `houses`
--

DROP TABLE IF EXISTS `houses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `houses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gambar` json DEFAULT NULL,
  `id_property` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `harga` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tipe` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `panjang` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `listrik` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sertifikat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `terdaftar` date NOT NULL,
  `kamar_tidur` int(11) NOT NULL,
  `kamar_mandi` int(11) NOT NULL,
  `ruang_tamu` int(11) NOT NULL,
  `dapur` int(11) NOT NULL,
  `denah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_parkir` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sudah_ditempati` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `locations_id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `houses`
--

LOCK TABLES `houses` WRITE;
/*!40000 ALTER TABLE `houses` DISABLE KEYS */;
INSERT INTO `houses` VALUES (38,'Om Arip','\"[\\\"uploads\\\\/property\\\\/img6.jpg\\\",\\\"uploads\\\\/property\\\\/img7.jpg\\\",\\\"uploads\\\\/property\\\\/img8.jpg\\\"]\"','P10002V','123131231231231','Jl. Paradise Rasamala 3 1950 Babakan Setu Kota Tangerang Selatan Banten 15315','Rumah',12,12,'12','1','2019-07-20',12,12,12,12,'uploads/denah/img1.jpg','1','1','adadwdawda',1,2),(39,'Alambana Siregar M.M.',NULL,'8','66814605','Jr. Padma No. 388, Batam 65961, Aceh','Flat',64,112,'1073','1','2019-07-20',2,9,3,1,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(40,'Dacin Jarwa Rajata S.Gz',NULL,'7','7507892','Jln. Untung Suropati No. 823, Lhokseumawe 21412, SulSel','Flat',89,109,'1295','0','2019-07-20',6,6,5,8,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(41,'Eka Putu Hutasoit S.Sos',NULL,'9','7396119','Jr. Bhayangkara No. 335, Administrasi Jakarta Timur 95261, Maluku','Flat',51,136,'1431','1','2019-07-20',1,5,3,4,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(42,'Eva Oliva Laksmiwati M.M.',NULL,'9','51745535','Ki. Cihampelas No. 517, Kediri 91864, KepR','Flat',109,82,'1124','1','2019-07-20',7,4,4,3,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(43,'Kasusra Utama Lazuardi S.IP',NULL,'13','14421242','Kpg. R.E. Martadinata No. 600, Singkawang 72666, JaTeng','Flat',124,107,'1256','0','2019-07-20',1,4,5,5,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(44,'Ikin Dongoran',NULL,'11','98939898','Gg. Adisucipto No. 954, Magelang 79998, KalUt','Flat',147,93,'1416','0','2019-07-20',8,7,9,7,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(45,'Widya Fujiati',NULL,'13','34733904','Jln. Dr. Junjunan No. 583, Bau-Bau 85281, SumSel','Flat',148,62,'1409','1','2019-07-20',6,10,6,1,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(46,'Kemal Hidayanto S.Psi',NULL,'7','79251562','Jr. Tambun No. 688, Binjai 78767, SulTeng','Flat',83,74,'1048','0','2019-07-20',4,2,4,6,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(47,'Melinda Maria Mayasari',NULL,'13','74146393','Psr. Juanda No. 244, Tidore Kepulauan 56156, Maluku','Flat',55,54,'1203','1','2019-07-20',4,5,9,3,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(48,'Ella Rahmawati S.T.',NULL,'9','34889896','Ds. Siliwangi No. 466, Tomohon 45991, MalUt','Flat',92,101,'1096','0','2019-07-20',2,7,7,5,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(49,'Nilam Winarsih S.Sos',NULL,'14','20805513','Jr. Tubagus Ismail No. 798, Pontianak 81890, Aceh','Flat',136,55,'1127','1','2019-07-20',2,7,4,5,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(50,'Maya Widiastuti S.Psi',NULL,'8','22155617','Gg. Salam No. 310, Sorong 72897, KalSel','Flat',147,108,'1344','0','2019-07-20',2,3,9,4,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(51,'Baktianto Firmansyah M.Pd',NULL,'7','22324634','Jln. Babadak No. 855, Langsa 13172, Gorontalo','Flat',98,83,'1389','1','2019-07-20',6,9,10,7,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(52,'Siti Mandasari',NULL,'6','62398428','Jr. Fajar No. 491, Cimahi 63974, KalSel','Flat',141,61,'1112','1','2019-07-20',9,6,5,7,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(53,'Violet Febi Rahmawati S.Pt',NULL,'11','85074740','Dk. Daan No. 816, Singkawang 72727, DKI','Flat',109,130,'1033','1','2019-07-20',8,10,4,5,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(54,'Najwa Padmi Andriani',NULL,'8','93760914','Dk. Babadan No. 128, Bau-Bau 57871, JaTim','Flat',119,107,'1407','0','2019-07-20',7,5,7,9,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(55,'Sari Lidya Suartini S.Pd',NULL,'14','7867184','Dk. Otto No. 236, Medan 57252, SumUt','Flat',106,130,'1279','1','2019-07-20',4,1,1,7,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(56,'Yulia Kasiyah Nuraini S.E.',NULL,'14','16670788','Ki. Merdeka No. 946, Kediri 39413, SumBar','Flat',103,72,'1311','0','2019-07-20',7,7,5,3,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(57,'Azalea Hassanah',NULL,'12','36663912','Gg. Bakau Griya Utama No. 798, Batu 34290, KalBar','Flat',121,75,'1163','0','2019-07-20',4,3,7,6,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(58,'Vanya Puspasari',NULL,'11','2224683','Ds. Mahakam No. 609, Salatiga 83685, SulUt','Flat',107,86,'1430','1','2019-07-20',1,6,3,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(59,'Dimaz Prasasta',NULL,'14','2106622','Ds. Thamrin No. 955, Sukabumi 17712, KalBar','Flat',65,114,'1288','1','2019-07-20',8,2,4,6,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(60,'Cornelia Indah Melani',NULL,'14','50907439','Ki. Tambun No. 590, Lhokseumawe 75996, Banten','Flat',59,87,'1231','1','2019-07-20',8,4,7,2,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(61,'Eluh Firmansyah S.Farm',NULL,'9','10176102','Ds. Suharso No. 635, Manado 90147, NTB','Flat',103,113,'1222','0','2019-07-20',2,6,5,3,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(62,'Edi Pratama',NULL,'5','31164508','Gg. Baya Kali Bungur No. 895, Pagar Alam 50027, Gorontalo','Flat',150,112,'1265','0','2019-07-20',8,5,6,1,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(63,'Gaduh Mursita Kuswoyo',NULL,'6','62439762','Gg. Salatiga No. 257, Sukabumi 63257, Jambi','Flat',83,116,'1299','1','2019-07-20',9,8,3,5,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(64,'Puput Purnawati',NULL,'14','97611659','Ki. Ciwastra No. 400, Banjarmasin 66240, SulTeng','Flat',67,95,'1478','1','2019-07-20',5,10,5,8,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(65,'Violet Fujiati',NULL,'15','85465105','Ds. Cemara No. 726, Cimahi 66543, PapBar','Flat',107,112,'1033','1','2019-07-20',9,2,9,4,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(66,'Vicky Oktaviani',NULL,'9','84067784','Ki. Kyai Gede No. 109, Mataram 99235, Maluku','Flat',113,60,'1408','1','2019-07-20',5,4,5,3,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(67,'Catur Januar',NULL,'7','64706194','Ds. Imam Bonjol No. 976, Bitung 80781, KalBar','Flat',144,115,'1140','0','2019-07-20',7,2,2,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(68,'Mursinin Hidayanto',NULL,'12','90960585','Kpg. Ikan No. 519, Manado 70830, SumBar','Flat',89,131,'1106','1','2019-07-20',5,5,10,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(69,'Rusman Pradana',NULL,'13','93606747','Dk. Suharso No. 478, Administrasi Jakarta Utara 37131, BaBel','Flat',130,60,'1414','1','2019-07-20',3,8,1,8,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(70,'Embuh Edward Wasita',NULL,'5','70196605','Jr. Bahagia No. 99, Bitung 65180, SumSel','Flat',102,76,'1150','1','2019-07-20',4,4,8,2,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(71,'Melinda Usada',NULL,'9','76536335','Jr. Otista No. 779, Bukittinggi 70355, MalUt','Flat',113,79,'1289','1','2019-07-20',8,3,7,3,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(72,'Janet Purwanti S.E.I',NULL,'8','72012522','Gg. Bappenas No. 91, Sabang 54022, Papua','Flat',135,58,'1066','1','2019-07-20',8,4,1,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(73,'Violet Aryani M.TI.',NULL,'11','44680021','Jln. Baranang No. 144, Tidore Kepulauan 73960, KalBar','Flat',137,90,'1454','0','2019-07-20',2,7,2,2,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(74,'Eka Melani',NULL,'7','43663693','Ki. Diponegoro No. 950, Malang 84484, KalTeng','Flat',74,110,'1263','0','2019-07-20',3,5,2,4,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(75,'Cahyanto Prayogo Hardiansyah',NULL,'5','49383902','Gg. Laksamana No. 540, Tual 26306, SulUt','Flat',75,76,'1386','0','2019-07-20',4,7,4,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(76,'Natalia Nadia Aryani',NULL,'14','30274392','Jln. Baha No. 426, Pontianak 32374, NTT','Flat',64,110,'1479','0','2019-07-20',6,9,4,6,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(77,'Jarwa Marbun',NULL,'13','2820195','Jln. Wora Wari No. 864, Denpasar 36814, SulTra','Flat',116,75,'1336','0','2019-07-20',3,6,2,2,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(78,'Johan Jono Kusumo S.E.',NULL,'8','96320920','Ds. Rajiman No. 25, Parepare 22311, JaTeng','Flat',114,137,'1451','1','2019-07-20',2,3,6,5,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(79,'Mutia Palastri',NULL,'9','31033756','Psr. Otto No. 292, Pariaman 21181, SulBar','Flat',73,65,'1240','1','2019-07-20',10,8,9,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(80,'Akarsana Adriansyah S.Pt',NULL,'5','82145129','Ds. Jambu No. 791, Probolinggo 69555, BaBel','Flat',96,62,'1305','0','2019-07-20',8,2,2,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(81,'Gabriella Lestari',NULL,'10','54077759','Psr. Rajiman No. 441, Tanjung Pinang 89694, BaBel','Flat',96,91,'1113','0','2019-07-20',7,8,4,6,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(82,'Adiarja Prakasa M.Kom.',NULL,'6','13618647','Gg. Babadak No. 382, Gunungsitoli 55859, JaTim','Flat',91,87,'1420','1','2019-07-20',7,4,3,4,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(83,'Labuh Sihotang',NULL,'14','16398326','Jr. Sudirman No. 470, Batu 24690, Riau','Flat',127,93,'1381','0','2019-07-20',8,6,4,10,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(84,'Karen Hamima Laksita',NULL,'11','81693398','Kpg. Samanhudi No. 401, Kediri 48614, SulTeng','Flat',106,60,'1498','0','2019-07-20',3,2,6,5,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(85,'Kala Situmorang',NULL,'7','54118289','Jr. Veteran No. 124, Administrasi Jakarta Selatan 84320, NTB','Flat',125,53,'1458','1','2019-07-20',3,9,4,8,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(86,'Samiah Haryanti',NULL,'11','94432093','Jr. Basmol Raya No. 644, Tangerang Selatan 67599, MalUt','Flat',118,74,'1399','1','2019-07-20',8,3,1,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(87,'Ellis Nuraini M.M.',NULL,'9','4239962','Jln. Uluwatu No. 310, Langsa 55206, SumUt','Flat',99,139,'1091','0','2019-07-20',5,3,2,8,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(88,'Adika Saptono M.Kom.',NULL,'7','50625857','Psr. Baranang Siang Indah No. 602, Sorong 14674, Lampung','Flat',127,117,'1436','1','2019-07-20',10,4,3,7,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(89,'Opan Ozy Firgantoro',NULL,'13','70834117','Ki. Jaksa No. 355, Medan 68172, KalUt','Flat',108,67,'1041','0','2019-07-20',5,10,8,5,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(90,'Puji Novitasari',NULL,'9','13444955','Ds. Pasirkoja No. 191, Metro 78231, Riau','Flat',127,95,'1377','0','2019-07-20',5,8,4,9,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(91,'Cengkir Pangestu',NULL,'13','17063864','Kpg. Gardujati No. 741, Pasuruan 77587, KalBar','Flat',99,77,'1463','0','2019-07-20',9,7,2,2,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(92,'Cinta Susanti',NULL,'5','79811527','Kpg. Ketandan No. 597, Batu 62593, JaTeng','Flat',94,103,'1042','0','2019-07-20',4,1,2,1,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(93,'Juli Febi Puspita',NULL,'5','52922211','Psr. Bak Mandi No. 253, Batu 31410, Bali','Flat',130,131,'1194','0','2019-07-20',7,9,10,3,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(94,'Dalimin Aditya Wasita',NULL,'15','8516847','Ki. Lumban Tobing No. 85, Sukabumi 30713, Banten','Flat',129,67,'1454','1','2019-07-20',5,1,7,3,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(95,'Eli Ana Padmasari',NULL,'10','60564518','Ki. Mulyadi No. 841, Banda Aceh 83388, DIY','Flat',62,136,'1406','1','2019-07-20',3,10,4,5,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(96,'Dalimin Rajata',NULL,'9','53462867','Dk. Sam Ratulangi No. 616, Gunungsitoli 56015, SulTra','Flat',118,145,'1461','0','2019-07-20',7,9,4,8,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(97,'Arsipatra Paiman Maulana',NULL,'14','71130027','Gg. Acordion No. 279, Banjar 43710, SulTra','Flat',147,110,'1438','0','2019-07-20',8,2,7,10,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(98,'Hani Julia Yulianti S.IP',NULL,'5','53848882','Ds. Fajar No. 866, Pekalongan 10814, SulUt','Flat',101,64,'1071','0','2019-07-20',5,8,4,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(99,'Tedi Mahendra',NULL,'6','79229328','Gg. Bass No. 818, Lubuklinggau 18353, KalTeng','Flat',125,62,'1040','0','2019-07-20',10,1,9,7,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(100,'Ganda Ega Mangunsong M.Ak',NULL,'8','55126677','Jln. Sutan Syahrir No. 425, Surakarta 92014, MalUt','Flat',111,117,'1225','0','2019-07-20',8,7,1,9,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(101,'Ghaliyati Suryatmi',NULL,'7','40773794','Ki. Suniaraja No. 456, Tangerang Selatan 66876, Papua','Flat',145,78,'1003','1','2019-07-20',9,7,1,8,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(102,'Kartika Haryanti S.Kom',NULL,'12','12294506','Ds. Urip Sumoharjo No. 48, Samarinda 39677, SumBar','Flat',123,147,'1130','1','2019-07-20',8,7,2,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(103,'Diah Raina Oktaviani S.Sos',NULL,'13','2706813','Dk. Jakarta No. 544, Probolinggo 97221, JaTim','Flat',63,105,'1405','1','2019-07-20',4,3,1,10,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(104,'Widya Putri Suartini S.Psi',NULL,'8','82058514','Psr. Suniaraja No. 769, Solok 53070, Jambi','Flat',149,133,'1100','0','2019-07-20',6,9,1,6,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(105,'Zelda Melani',NULL,'5','74067378','Dk. Bagas Pati No. 995, Salatiga 59581, JaBar','Flat',73,122,'1329','0','2019-07-20',2,9,3,5,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(106,'Cakrawangsa Hidayanto S.Farm',NULL,'8','78543835','Kpg. Hang No. 478, Magelang 84543, Bali','Flat',139,77,'1415','0','2019-07-20',7,5,9,6,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(107,'Rina Endah Zulaika',NULL,'6','51170251','Psr. Flores No. 646, Administrasi Jakarta Utara 94757, KepR','Flat',108,106,'1253','0','2019-07-20',1,8,9,9,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(108,'Cakrabirawa Kacung Firgantoro',NULL,'10','77827923','Dk. Reksoninten No. 751, Tanjungbalai 53670, KalTim','Flat',114,80,'1342','0','2019-07-20',3,5,4,10,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(109,'Omar Zulkarnain S.I.Kom',NULL,'15','45898125','Psr. Padang No. 517, Malang 10613, Gorontalo','Flat',125,118,'1395','0','2019-07-20',9,7,6,4,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(110,'Nardi Habibi',NULL,'14','44696646','Gg. Babakan No. 537, Pekalongan 20265, NTT','Flat',75,122,'1454','0','2019-07-20',8,7,3,5,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(111,'Sakura Laksita',NULL,'8','96752283','Jr. Halim No. 199, Tual 76763, BaBel','Flat',145,114,'1396','0','2019-07-20',1,8,6,1,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(112,'Ghaliyati Laksmiwati',NULL,'13','12963045','Dk. Fajar No. 633, Samarinda 92035, Aceh','Flat',143,80,'1013','1','2019-07-20',6,6,3,6,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(113,'Puput Yuniar',NULL,'6','96359480','Jr. Baing No. 460, Manado 23004, SulTeng','Flat',75,115,'1108','0','2019-07-20',7,9,8,9,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(114,'Kenes Prabowo',NULL,'10','51053597','Jln. PHH. Mustofa No. 66, Parepare 69638, KepR','Flat',117,69,'1407','0','2019-07-20',7,2,8,9,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(115,'Ida Umi Mayasari',NULL,'12','80628535','Jr. B.Agam 1 No. 965, Tarakan 25175, Papua','Flat',144,125,'1417','0','2019-07-20',10,9,6,4,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(116,'Hani Rahayu',NULL,'5','16814155','Kpg. Jakarta No. 905, Blitar 94948, DIY','Flat',74,63,'1244','0','2019-07-20',8,6,3,9,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(117,'Tina Siska Agustina',NULL,'8','9339284','Jln. Dahlia No. 701, Magelang 10950, SulTra','Flat',145,95,'1023','1','2019-07-20',8,2,8,6,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(118,'Atma Saragih',NULL,'12','8627348','Jln. Bakti No. 288, Yogyakarta 16715, Bengkulu','Flat',60,127,'1176','0','2019-07-20',1,3,5,9,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(119,'Asman Halim',NULL,'14','30347070','Jln. Basmol Raya No. 838, Pekanbaru 30595, KalUt','Flat',69,63,'1468','0','2019-07-20',6,6,2,7,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(120,'Usyi Laras Utami',NULL,'11','73914346','Jr. Bappenas No. 160, Palu 76410, DKI','Flat',145,79,'1253','1','2019-07-20',3,4,9,6,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(121,'Kunthara Irawan S.Pd',NULL,'6','6972561','Jr. Untung Suropati No. 231, Tanjungbalai 99766, KalSel','Flat',62,99,'1433','1','2019-07-20',3,6,9,7,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(122,'Jarwadi Suwarno S.Sos',NULL,'13','49064357','Jln. Warga No. 737, Tebing Tinggi 43249, Banten','Flat',141,117,'1053','0','2019-07-20',8,8,2,2,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(123,'Yuliana Putri Hariyah S.Psi',NULL,'10','51058166','Ds. Sugiyopranoto No. 71, Sungai Penuh 43491, BaBel','Flat',117,123,'1286','1','2019-07-20',1,1,10,1,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(124,'Hasta Wibisono',NULL,'11','26569510','Ki. Sampangan No. 928, Semarang 14336, MalUt','Flat',118,73,'1198','0','2019-07-20',2,7,7,2,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(125,'Ulva Uyainah S.Kom',NULL,'9','88868695','Ds. Dipatiukur No. 54, Lhokseumawe 49638, SulBar','Flat',127,64,'1032','1','2019-07-20',6,6,7,7,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(126,'Mariadi Wibowo',NULL,'11','63315667','Ds. Lumban Tobing No. 833, Administrasi Jakarta Utara 70625, KalTeng','Flat',131,91,'1367','0','2019-07-20',2,4,2,5,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(127,'Jati Waskita',NULL,'14','31917456','Jr. Suryo No. 451, Sibolga 74143, JaBar','Flat',111,132,'1079','0','2019-07-20',10,9,8,8,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(128,'Natalia Sari Hasanah',NULL,'6','57131177','Jr. Wahidin Sudirohusodo No. 810, Tidore Kepulauan 33536, SulBar','Flat',86,109,'1334','1','2019-07-20',3,3,6,7,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(129,'Violet Aryani',NULL,'10','64690085','Psr. Moch. Ramdan No. 622, Gorontalo 42583, NTT','Flat',146,139,'1047','0','2019-07-20',6,4,6,7,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(130,'Cengkir Tampubolon',NULL,'12','19618756','Dk. Gading No. 513, Jayapura 99349, DKI','Flat',128,77,'1243','1','2019-07-20',3,7,8,4,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(131,'Gambira Raharja Prayoga',NULL,'9','72977009','Gg. Baan No. 96, Administrasi Jakarta Selatan 62387, JaTim','Flat',104,107,'1017','1','2019-07-20',4,3,9,2,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(132,'Cahyadi Nababan S.Gz',NULL,'12','77830246','Ds. Bakau Griya Utama No. 227, Banjar 73273, Jambi','Flat',147,74,'1349','1','2019-07-20',4,6,4,2,NULL,'1','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(133,'Asmianto Mahdi Jailani M.Pd',NULL,'10','55761327','Ds. Sukajadi No. 733, Lubuklinggau 86159, KalUt','Flat',66,73,'1452','1','2019-07-20',9,4,4,10,NULL,'1','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(134,'Kanda Anggriawan',NULL,'14','94681438','Jln. Bata Putih No. 976, Singkawang 42504, DIY','Flat',100,122,'1159','0','2019-07-20',1,3,2,3,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(135,'Jono Adriansyah',NULL,'12','47515367','Ki. Gajah Mada No. 680, Bandar Lampung 87666, Bengkulu','Flat',141,137,'1360','0','2019-07-20',7,5,9,6,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(136,'Cahyo Najmudin M.M.',NULL,'7','9277408','Jr. Gading No. 785, Pasuruan 74445, BaBel','Flat',90,135,'1353','1','2019-07-20',10,7,6,10,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(137,'Restu Samiah Kusmawati S.Gz',NULL,'10','68150162','Dk. Hasanuddin No. 863, Palu 64297, BaBel','Flat',70,127,'1452','0','2019-07-20',5,6,1,9,NULL,'0','0','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2),(138,'Padmi Pudjiastuti',NULL,'14','55009166','Dk. Hang No. 512, Medan 16684, KalSel','Flat',79,60,'1469','1','2019-07-20',3,7,7,2,NULL,'0','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',1,2);
/*!40000 ALTER TABLE `houses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Jakarta'),(2,'Tangerang Selatan');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(7,'2019_06_14_181405_create_houses_table',2),(8,'2019_06_15_053317_create_locations_table1',2),(9,'2019_06_16_082057_create_articles_table',3),(10,'2019_06_16_082525_create_galleries_table',3),(11,'2019_06_16_082605_create_contacs_table',3),(12,'2019_06_16_083000_create_homes_table',3),(13,'2016_08_07_145904_add_table_cms_apicustom',4),(14,'2016_08_07_150834_add_table_cms_dashboard',4),(15,'2016_08_07_151210_add_table_cms_logs',4),(16,'2016_08_07_151211_add_details_cms_logs',4),(17,'2016_08_07_152014_add_table_cms_privileges',4),(18,'2016_08_07_152214_add_table_cms_privileges_roles',4),(19,'2016_08_07_152320_add_table_cms_settings',4),(20,'2016_08_07_152421_add_table_cms_users',4),(21,'2016_08_07_154624_add_table_cms_menus_privileges',4),(22,'2016_08_07_154624_add_table_cms_moduls',4),(23,'2016_08_17_225409_add_status_cms_users',4),(24,'2016_08_20_125418_add_table_cms_notifications',4),(25,'2016_09_04_033706_add_table_cms_email_queues',4),(26,'2016_09_16_035347_add_group_setting',4),(27,'2016_09_16_045425_add_label_setting',4),(28,'2016_09_17_104728_create_nullable_cms_apicustom',4),(29,'2016_10_01_141740_add_method_type_apicustom',4),(30,'2016_10_01_141846_add_parameters_apicustom',4),(31,'2016_10_01_141934_add_responses_apicustom',4),(32,'2016_10_01_144826_add_table_apikey',4),(33,'2016_11_14_141657_create_cms_menus',4),(34,'2016_11_15_132350_create_cms_email_templates',4),(35,'2016_11_15_190410_create_cms_statistics',4),(36,'2016_11_17_102740_create_cms_statistic_components',4),(37,'2017_06_06_164501_add_deleted_at_cms_moduls',4),(38,'2019_07_07_205458_add_column_houses',5),(39,'2019_07_09_093936_drop_contacs_table',6),(40,'2019_07_09_095501_create_contacts_table',7),(41,'2019_07_14_220632_add_column_articles',8),(42,'2019_07_14_222854_remove_column_houses_from_houses',9),(43,'2019_07_14_223604_add_column_houses',10),(44,'2019_07_14_223633_add_column_articles',11);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('arif.murdiyono2107@gmail.com','b9d4f9c203b701a43bfb24cd82b7ceaae16b7a8b769dba25111804b87296644a','2019-06-14 11:11:23');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'parul','paru@mail.com','$2y$10$MvfEx9kpw0qKNxZoxtAOqOD4Wuqq/NJweBj4GI0filB7sBmTkXuwq','KHL0PUNnwZDUyqNhYt1hvqPRARK438EqT1q6lEYXu9nyiK5LuD8wpm0cAjvT','2019-06-14 11:10:15','2019-06-14 11:10:22'),(2,'arif','arif.murdiyono2107@gmail.com','$2y$10$ptn/l/bdu66q4cWVCdAJveuH627Ini7.DPP92ee3araUnEj18tapK','ngTLH9PlTmXUt5eCdfdVJua1IQBNJq1RV6VDougTVnMdsWCROLuEKFsyLi25','2019-06-14 11:10:46','2019-06-14 11:10:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-21  1:53:09
