<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 100; $i++){
 
            // insert data ke table pegawai menggunakan Faker
            DB::table('houses')->insert([
                'gambar' => NULL,
                'id_property' => rand(5, 15),
                'nama' => $faker->name,
                'harga' => $faker->randomNumber(8),
                'alamat' => $faker->address,
                'tipe' => 'Flat',
                'panjang' => rand(50, 150),
                'lebar' => rand(50, 150),
                'listrik' => rand(1000, 1500),
                'sertifikat' => rand(0, 1),
                'terdaftar' => date('Y-m-d'),
                'kamar_tidur' => rand(1, 10),
                'kamar_mandi' => rand(1, 10),
                'dapur' => rand(1, 10),
                'denah' => NULL,
                'tempat_parkir' => rand(0, 1),
                'sudah_ditempati' => rand(0, 1),
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor',
                'locations_id' => 1,
                'contacts_id' => 2,
                'ruang_tamu' => rand(1, 10),
            ]);
 
    	}
    }
}
