<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('gambar');
            $table->string('id_property');
            $table->string('harga');
            $table->string('alamat');
            $table->string('tipe');
            $table->integer('panjang');
            $table->integer('lebar');
            $table->string('listrik');
            $table->string('sertifikat');
            $table->date('terdaftar');
            $table->integer('kamar_tidur');
            $table->integer('kamar_mandi');
            $table->integer('ruang_tamu');
            $table->integer('dapur');
            $table->string('denah');
            $table->string('tempat_parkir');
            $table->string('sudah_ditempati');
            $table->string('deskripsi');
            $table->integer('locations_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacs_id');
    }
}
