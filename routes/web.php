<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//route untukbkhup8o9 rumah

Route::get('/', 'HomesController@showHomes')->name('home');


//HouseRoute
Route::get('/property-list', 'HouseController@showPropertyList')->name('list.house');
Route::get('/property/{id}', 'HouseController@show')->name('show.house');
Route::get('/property-grid', 'HouseController@showPropertyGrid')->name('grid.house');
Route::get('/property/get/list/all', 'HouseController@getPropertyListAll');
Route::get('/property/get/grid/all', 'HouseController@getPropertyGridAll');
//link untuk nambahin property
Route::post('/property/add', 'HouseController@store')->name('property.store');

//GalleryRoute
Route::get('/gallery', 'GalleryController@showGallery')->name('index.gallery');

//ArticleRoute
Route::get('/article', 'ArticleController@showArticle')->name('index.article');
Route::get('/article/{id}', 'ArticleController@show')->name('show.article');

//ContactRoute
Route::get('/contact', 'ContactController@showContact')->name('index.contact');




Auth::routes();
